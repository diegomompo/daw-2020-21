D = 2; // Variable para sustituir al 2 en las operaciones
M = 0.5; // Varibale para sustituir a 0.5 en las operaciones

sum_mediana = 0; // Variable para sumar los numeros centrales de la mediana
res_mediana; // Variable para recoger la mediana
var igual_mediana; // Variable para calcular el número de la celda de la mediana

function mediana(array){ // Función para calcular la mediana

    if((array.length % D) == 0){ // Compruebo si la longitud es par o impar

        for(i=0; i<array.length; i++){ // Recorro el array para buscar los dos números centrales si la longitud es par

            if(i >= ((array.length / D)-1) && i<= (array.length / D)){ // Busco las celdas centrales del array
                sum_mediana += array[i]; // sumo los dos números centrales
            }
        }
        res_mediana = sum_mediana / D; // Divido la suma de los dos números centrales por la mitad

    }
    else if((array.length %  D) != 0){ // Compruebo si la longitud es par o impar

        igual_mediana = (array.length / D) - M; // Divido la longitud entre dos y luego le resto 0,5 para saber el número de celda de la mediana
        
        for(i=0; i<array.length;i++){ // Recorro el array para buscar la mediana qu esta en la celda correspondiente
            if(i == igual_mediana){
                res_mediana = array[i]; //Guardo la mediana en una variable
            }
        }

    }
    return res_mediana; // Devuelvo la mediana
}