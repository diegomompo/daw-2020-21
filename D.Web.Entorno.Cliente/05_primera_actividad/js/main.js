var res_mediana; // Resultado de la mediana

window.onload = function(){
    $(document).ready(MediaAJAX);
}

function MediaAJAX(){
    let divMedia = $("<div>");
    divMedia.attr("id", divMedia);
    $("body").append(divMedia);

    medMedia();
}

function medMedia(){
    $.ajax({
        'type': 'GET',
        'url': "http://127.0.0.1:5500/Mates.json",
        "async": true,
    }).done(mostrarMedia);
}

function mostrarMedia(objetoJSONMedia){

    arrayMedia = objetoJSONMedia.variables;

    $.each(arrayMedia, function(i, variables){

    variables.N = variables.numberN; // Declaro N igual a 10 para usar la letra y no el número posteriormente

    $variables.resAritmetica; // Resultado de la media aritmetica

    variables.stdin;

    numeros = new Array(variables.N); // Array donde se meterá los números introducidos por el usuario

    document.getElementById("titulo").innerHTML = "Actividad: Media artitmética, mediana y moda en JavaScript"; //Mensaje cabecera


    for(var i=0; i<variables.N; i++){ //Recorro el array vacio para rellenarlo con números
        var num = prompt("Introduce un número en la celda " + i); // Se pide al usuario que introduzca el numero en la celda correspondiente y el usuario lo hace
        var numero = parseInt(num) // Se convierte numero introducido en formato entero
       numeros[i] = numero; // se guarda el número introducido en la celda que corresponde
    }

    console.log("Array normal: ");

    for(var  i = 0; i<numeros.length; i++){
        console.log(numeros[i]); // Muestra el array el normal
    }

    for(var i=numeros.length-1; i>=0; i--){ // Recorro el array para eliminar todos los número que sean menores que 1 o mayores que 100
        if(numeros[i] < 1 || numeros[i] > 100){ //Comparo si el numero que esta en la celda es menor que 1 o mayor que 100
            numeros.splice(i, 1); //extrae el número de la celda correspondiente y todas las celdas  se mueven hacia la izquierda
        }
    } 
    
    console.log("Array sin 0 y 100: ");

    for(var i= 0 ; i<numeros.length; i++){

        console.log(numeros[i]); // Muestra el array sin los numeros menores que 0 mayores que 100
     
}
function ordenar_num(num1, num2){ // Función para odernar los números de menor a mayor.
    return num1 - num2; // Devuelve el resultado de la operación. Si devuelve positivo, el segundo numero tiene que ir antes que el primero.
                        //Si devuelve cero, los dos numeros tienen el mismo orden
                        // si devuelve negativo, el primer número va antes que el segundo 
}

    
    numeros.sort(ordenar_num); // Ordena los números de menor a mayor numéricamente y no alfabéticamente

    console.log("Array ordenado: ");
 
    for(var  i = 0; i<numeros.length; i++){
        console.log(numeros[i]); // Muestra el array ordenado numéricamente de menor a mayor
    }

    variables.resAritmetica = aritmetica(numeros); // Devuelve el resultado de la la media artimetica y lo guarda en la variable
    res_mediana = mediana(numeros); //Devuelve el resultado de la mediana y lo guarda en la variable
    moda(numeros); // Llama a la función que calcula la moda

        document.getElementById("pulsa").innerHTML = "Pulsa F12 para ver los resultados";
        console.log("La media aritmetica del array es " +  variables.res_Aritmetica); // Imprimo el resultado final de la media aritmetica
        console.log("La mediana del array es " +  res_mediana); // Imprimo el resultado final de la mediana 
    })
}