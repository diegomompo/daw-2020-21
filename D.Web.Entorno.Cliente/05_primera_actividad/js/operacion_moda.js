repeticiones_moda = 1; // Variable que tendrá el número de repeticiones actual
repeticiones_max_moda = 1; // Variable que tendrá el número de repeticiones máximo
var repeticiones_max_anterior1 = 1; // Variable que recogerá el número de repeticiones de una anterior moda
var repeticiones_max_anterior2 = 1; // Variable que recogerá el número de repeticiones de una anterior moda
var moda_anterior1; // Moda anterior
var moda_anterior2; // Moda anterior
var res_moda; // Moda actual

function moda(array) { // Función que calcula la moda

    res_moda = moda_anterior1 = array[0]; // Igual la moda actual al primer valor del array


    for(var i=0; i<array.length;i++){
        if(array[i] == array[i+1]){ // Si las celdas que se comparan son iguales, se uma una repeteción
            repeticiones_moda += 1;

        }
        else { // Si el las celdas que se comparan no son iguales, entonces se se compara si el numero de repeteciones actual es mayor o igual al que estaba
            if(repeticiones_moda >= repeticiones_max_moda){ //Si es así las repeticiones y las modas pasan a una variable anterior y 
                                                           //en la actual se guardan las nuevas repeticiones y la nueva moda

               repeticiones_max_anterior2 = repeticiones_max_anterior1; //La moda anterior se guarda en una nueva variable por si acaso coincide con las dos primeras
                repeticiones_max_anterior1 = repeticiones_max_moda; //El numero maximo de repeticiones actual para a una variable secundaria
                repeticiones_max_moda = repeticiones_moda; // El número de repeticiones de moda ahora es el número de repeticiones máximo
                repeticiones_moda = 1; //Se inicializa el numero de repeticiones a 1 
                moda_anterior2 = moda_anterior1; //La moda que estaba en la variable secundaria pasa a una tercera variable
                moda_anterior1 = res_moda; //La moda actual pasa a ser una moda secundaria
                res_moda = array[i]; //La moda actual es número que esté en la celda del array
            }
        }
    }

    if(repeticiones_max_moda == 1){ //Si no hay repeticiones significa que no hay moda
        console.log("No hay moda");
    }
    else if(repeticiones_max_anterior1 == repeticiones_max_moda){ // Si el número de repeticiones anterior coincide con el actua, es que hay dos modas
            if(repeticiones_max_anterior2 == repeticiones_max_anterior1){ // Si el número de repteciones anterior 2 es igual al anterior significa que hay tres modas
                console.log("Número de repticiones de las tres modas: " + repeticiones_max_moda ); //Imprime el número de repeteciones y las tres modas
                console.log("Primera moda: " + moda_anterior2 );
                console.log("Segunda moda: " + moda_anterior1 );
                console.log("Tercera moda: " + res_moda);

            }else{ //Imprime dos modas
               console.log("Número de repticiones de las dos modas: " + repeticiones_max_moda ); //Imprime el número de repeteciones y las dos modas
               console.log("Primera moda: " + moda_anterior1);
               console.log("segunda moda: " + res_moda);
        }
    }
    else{ // Imprime un moda

        console.log("Número de repticiones de la moda: " + repeticiones_max_moda ); //Imprime el número de repeteciones y la moda
        console.log("Moda: " + res_moda);
    }    
    // Por último se imprime todas las repteiciones de las modas correspondientes   
}