var precioIngedientes = 0; //Variable para sumar los ingredientes extras
var precioTamañoPizza = 0; //Variable para guardar el precio del tamaño de la pizza
var precioTotal = 0; //Importe total del pedido
var imprimePrecio = 0; //Variable que recoge el Importe total de la función

//FUNCION PARA PROCESAR EL PEDIDO E IMPRIMIR EL RESULTADO

function procesando() { //Función que procesa el pedido

    window.open("barra_procesando.html" , "ventana3" , "width=1920,height=150,scrollbars=NO"); // Barra de cargando que se abre en otra ventana y carga el procesamiento del pedido
    
    imprimePrecio = CalcularPrecio(); // Llama a la función que calcula el importe total del pedido

    window.open("barra_imprimiendo.html" , "ventana1" , "width=1920,height=150,scrollbars=NO"); // Barra de calculando el precio total

    if(imprimePrecio != 0){ //Comprueba si hay importe o no

        //Imprime el pedido completo

        alert("El precio total es " + precioTotal + " €");
        alert("Pulse F12 para consultar el pedido entero");

        console.log("Nombre: " + valorNombre.value);
        console.log("Dirección: " + valorDireccion.value);
        console.log("Teléfono: " + valorTelefono.value);
        console.log("Email: " + valorEmail.value);
        console.log("Pizza pequeña: " + pequena.checked);
        console.log("Pizza mediana: " + mediana.checked);
        console.log("Pizza grande: " + grande.checked);
        console.log("Precio del tamaño de la pizza: " + precioTamañoPizza);
        console.log("Jamón como Ingrediente: " + topping1.checked);
        console.log("Pepperoni como Ingrediente: " + topping2.checked);
        console.log("Queso como Ingrediente: " + topping4.checked);
        console.log("Salami como Ingrediente: " + topping5.checked);
        console.log("Tocino como Ingrediente: " + topping3.checked);
        console.log("Precio de los ingredientes extra : " + precioIngedientes);
        console.log("Precio total del pedido : " + precioTotal);
    }
}

// -------------------------------------------------------------------------

//FUNCION PARA CALCULAR EL PRECIO TOTAL DEL PEDIDO


function CalcularPrecio(){ //Función que calcula el precio total del pedido

    window.open("barra_calculando.html" , "ventana2" , "width=1920,height=150,scrollbars=NO"); // Barra de calculando el precio total

//-------------------------------------------------------------------------------

// CALCULA EL TAMAÑO DE LA PIZZA

tamano = document.getElementsByName("tamano");

for(var i=0; i<tamano.length;i++){
    if(tamano[i].checked){
        var valorTam = Number(tamano[i].value);
        precioTamañoPizza = valorTam;
        break;
    }
}

 
  //-----------------------------------------------------------------------

  //CALCULA EL PRECIO DE LOS INGREDIENTES

    topping = document.getElementsByName("topping");

    for(var i=0; i<topping.length;i++){
        if(topping[i].checked){
            var valorIng = Number(topping[i].value);
            precioIngedientes += valorIng;
        }
    }

//--------------------------------------------------------------------------------

// CALCULA EL PRECIO TOTAL DEL PEDIDO

    precioTotal = precioTamañoPizza + precioIngedientes; //Suma el precio del tamaño de la pizza y el precio de los ingredientes extra

}

// -------------------------------------------------------------------------

//FUNCION PARA LIMPIAR LOS CAMPOS DEL FORMUULARIO


function limpiar(){ //Función para limpiar los campos
    window.open("barra_reiniciando.html" , "ventana1" , "width=1920,height=150,scrollbars=NO"); // Barra de progreso que indica se está limpiando los campos
    validacion.disabled = true;
    document.getElementById("reiniciar").reset(); //nodo que se llama desde el form para limpiarlo
}