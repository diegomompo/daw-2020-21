var devolverErrorCampo = new Boolean();
var devolverErrorTamano = new Boolean();
var devolverErrorExtra= new Boolean();

function validar(){
    window.open("barra_validando.html" , "ventana1" , "width=1920,height=150,scrollbars=NO"); 
    comprobarErrorCampo();
    comprobarErrorTamano();
    comprobarErrorExtra();
    comprobarProceso();
}


// FUNCION QUE COMRPUEBA SI TODOS LOS CAMPOS SON VÁLIDOS

function comprobarErrorCampo(){


    comprobarNombre = /^[a-z]./;
    comprobarTelefono = /^\d{9}$/;
    comprobarEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*\.{1,125}[A-Z]{2,63}$/i;

    //COMPROBAR EL CAMPO NOMBRE
     
    if(valorNombre.value.trim() == ''){ 
        
        alert("El campo nombre es obligatorio");
        devolverErrorCampo = true;

    }
    else if(comprobarNombre.test(valorNombre.value)){
            alert("El nombre tiene el formato incorrecto");
            devolverErrorCampo = true;
    }

    // COMPROBAR EL CAMPO DIRECCIÓN

    else if(valorDireccion.value.trim() == ''){ 
        
        alert("EL campo direccion es obligatorio")
        devolverErrorCampo=true;

    }

    // COMPROBAR EL CAMPO TELÉFONO

    else if(valorTelefono.value.trim() == ''){
        
        alert("EL campo teléfono es obligatorio")
        devolverErrorCampo = true;

    }
    else if(!(comprobarTelefono.test(valorTelefono.value))){
        alert("EL campo teléfono tiene el formato incorrecto");
        devolverErrorCampo = true;
    }

    //COMPROBAR EL CAMPO EMAIL

    else if(valorEmail.value.trim() == ''){ 
        
        alert("EL campo email es obligatorio");
        devolverErrorCampo = true;

    }
    else {
        
        if(comprobarEmail.test(valorEmail.value)){
            //el nombre de usuario de correo y el nombre del servidor debe tener por lo menos una letra o número, se puede repetir mas de una vez y puede haber guiones y puntos
            devolverErrorCampo = false;
        }
        else{
            alert("El email tiene el formato incorrecto");
            devolverErrorCampo = true;

        }                        
    }
}

//---------------------------------------------------------------------------------------------------------------------------

//FUNCION QUE COMRPUEBA SI SE HA SELECCIONADO UN TAMAÑO DE PIZZA

function comprobarErrorTamano(){

    pequena = document.getElementsByName("pequena");
    mediana = document.getElementsByName("mediana");
    grande = document.getElementsByName("grande");


    if(pequena.checked == false){ // Comprueba si se ha seleccionado un tipo de tamaño de pizza
        if(mediana.checked == false){
            if(grande.checked == false){
                alert("Debe seleccionar el tamaño de la pizza");
                devolverErrorTamano = true;
            }
            else{
                devolverErrorTamano = false;
            }
        }
        else{
            devolverErrorTamano = false;
        }
    }
    else{
        devolverErrorTamano = false;
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------

//FUNCION PARA COMPROBAR LOS INGREDIENTES EXTRA

function comprobarErrorExtra(){

    topping = document.getElementsByName("topping");

    for(var i=0; i<topping.length;i++){
        if(topping[i].checked){
            devolverErrorExtra = false;
            break;
        }
        else{
            devolverErrorExtra = true;
        }
    }
    if(devolverErrorExtra == true){
        alert("Debe seleccionar un ingrediente extra por lo menos");
    }
    
}


//-----------------------------------------------------------------------------------------------------------------------------------------

//FUNCION PARA VALIDAR SI SE PUEDE PROCESAR EL PEDIDO O NO

function comprobarProceso() {
    if(devolverErrorCampo == false){
        if(devolverErrorTamano == false){
            if(devolverErrorExtra == false){

                alert("Todos los campos están verificados correctamente");
                validacion.disabled = false;
            }
        }
    }
}
