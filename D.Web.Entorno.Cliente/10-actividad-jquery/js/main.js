//--------------------------------------------------------------
var disponible = true;

//FUNCION QUE EJECUTA LAS DISTINTAS FUNCIONES QUE TENDRÁ EL HTML

window.onload = function(){
    
    $(document).ready(textoInicioPizzeria);
    $(document).ready(obligadoPizzeria);
    $(document).ready(formularioPizzeria);
}
//--------------------------------------------------------------------

//FUNCION PARA MOSTRAR EL NOMBRE DE LA TIENDA

function textoInicioPizzeria(){
        var header1 = $("<h1>");
        header1.text("Pizzería con JQuery");
        header1.addClass("nombrePizzeria")

        $("body").append(header1);
}
//----------------------------------------------------------------------

//FUNCION PARA MOSTRAR QUE LOS TEXTOS EN ROJO SON OBLIGATORIOS

function obligadoPizzeria(){

    var textObligatorio = $("<p>");
    textObligatorio.text("Los textos en rojo es obligatorio completarlos");
    $("body").append(textObligatorio);


}
//-------------------------------------------------------------------------

//FUNCION PARA INICIAR EL FORMULARIO

function formularioPizzeria(){
    var formuPizzeria = $("<form>");
    formuPizzeria.attr("id", "reiniciar");

    $("body").append(formuPizzeria);

    $(document).ready(nombre);
    $(document).ready(direccion);
    $(document).ready(telefono);
    $(document).ready(email);
    $(document).ready(mostrarTamanoPizza);
    $(document).ready(mostrarIngredientes);
    $(document).ready(buttonValidar);
    $(document).ready(buttonProcesar);
    $(document).ready(buttonLimpiar);


}



//---------------------------------------------------------------------------

//FUNCIÓN PARA EL CAMPO NOMBRE

function nombre(){  

    var leftNombre = $("<left>");
    leftNombre.text("Nombre: ");
    leftNombre.attr("class", "obligatorio");

    var campoNombre = $("<input>");
    campoNombre.attr("id", "ValorNombre");

    var br1 = $("<br>");
    var br2 = $("<br>");

    $("#reiniciar").append(leftNombre);
    $("#reiniciar").append(campoNombre);
    $("#reiniciar").append(br1);
    $("#reiniciar").append(br2);

}

//---------------------------------------------------------------------------

//FUNCIÓN PARA CAMPO DIRECCIÓN

function direccion(){

    var leftDireccion = $("<left>");
    leftDireccion.text("Dirección: ");
    leftDireccion.attr("class", "obligatorio");

    var campoDireccion = $("<input>");
    campoDireccion.attr("id", "ValorDireccion");

    var br3 = $("<br>");
    var br4 = $("<br>");

    $("#reiniciar").append(leftDireccion);
    $("#reiniciar").append(campoDireccion);
    $("#reiniciar").append(br3);
    $("#reiniciar").append(br4);

}

//------------------------------------------------------------------------------

//FUNCION PARA CAMPO TELEFONO

function telefono(){

        var leftTelefono = $("<left>");
    leftTelefono.text("Teléfono: ");
    leftTelefono.attr("class", "obligatorio");

    var campoTelefono = $("<input>");
    campoTelefono.attr("type", "number");
    campoTelefono.attr("id", "valorTelefono");

    var br5 = $("<br>");
    var br6 = $("<br>");

    $("#reiniciar").append(leftTelefono);
    $("#reiniciar").append(campoTelefono);
    $("#reiniciar").append(br5);
    $("#reiniciar").append(br6);

}

//---------------------------------------------------------------------------------

//FUNCION PARA CAMPO EMAIL

function email(){


    var leftEmail = $("<left>");
    leftEmail.text("Email: ");
    leftEmail.attr("class", "obligatorio");

    var campoEmail = $("<input>");
    campoEmail.attr("id", "ValorEmail");

    var br7 = $("<br>");
    var br8 = $("<br>");

    $("#reiniciar").append(leftEmail);
    $("#reiniciar").append(campoEmail);
    $("#reiniciar").append(br7);
    $("#reiniciar").append(br8);

}
//---------------------------------------------------------------------------------

//FUNCION TAMAÑO DE LA PIZZA
function  mostrarTamanoPizza(){

    var tamanodiv = $("<div>");
    tamanodiv.attr("id", "pizza");

    $("#reiniciar").append(tamanodiv);

    $(document).ready(tamanoPizza);
}

function tamanoPizza(){

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     $("#pizza").html(this.responseText);
    }
  };
  xhttp.open("GET", "http://127.0.0.1:5500/tamano_pizza.html", true);
  xhttp.send();
}


//-------------------------------------------------------------------------------------

// FUNCION TEXTO INGREDIENTES

function mostrarIngredientes(){

      var extradiv = $("<div>");
    extradiv.attr("id", "extra");

    $("#reiniciar").append(extradiv);

    $(document).ready(ingExtra);
}

function ingExtra(){

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        $("#extra").html(this.responseText);
    }
  };
  xhttp.open("GET", "http://127.0.0.1:5500/Ingredientes.html",true);
  xhttp.send();
}

//----------------------------------------------------------------------------------

//FUNCIÓN BOTÓN VALIDAR

function  buttonValidar(){

    var botonValidar = $("<button>");
    botonValidar.text("Validar");

    botonValidar.click(function(){
        validar();
    });

    $("body").append(botonValidar);

}

//----------------------------------------------------------------------------------

//FUNCIÓN BOTÓN ENVIAR

function  buttonProcesar(){

    var botonProcesar = $("<button>");
    botonProcesar.text("Procesar");

    botonProcesar.attr("disabled", "true");

    botonProcesar.attr("id", "validacion");

    botonProcesar.click(function(){
        mostrarPedido();
    });

    $("body").append(botonProcesar);

}

//-------------------------------------------------------------------------------------

//FUNCIÓN BOTÓN LIMPIAR

function  buttonLimpiar(){

  var botonLimpiar = $("<button>");
    botonLimpiar.text("Limpiar");

    botonLimpiar.click(function(){
       limpiar();
    });

    $("body").append(botonLimpiar);
}

//---------------------------------------------------------------------------------------

//FUNCION PARA PROCESAR EL PEDIDO
function mostrarPedido(){

    
    var divPrecio = $("<div>");
    divPrecio.attr("id", "precio");

    $("body").append(divPrecio);

     $(document).ready(realizando);

}

function realizando(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){

        if(this.readyState == 4 && this.status == 200){
            $("precio").html(this.responseText);
        }
    };
    xhttp.open("GET", "http://127.0.0.1:5500/pedido.html", true);
    xhttp.send();
    
    $(document).ready(procesando);
}





















































































