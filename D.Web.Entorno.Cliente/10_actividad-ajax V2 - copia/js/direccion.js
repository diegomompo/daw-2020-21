//FUNCIÓN PARA CAMPO DIRECCIÓN

function inicioDireccion(){
    var divDireccion = document.createElement("div");
    divDireccion.id = "idDireccion";

    document.getElementById("reiniciar").appendChild(divDireccion);

    direccion();

}


function direccion(){

    xhttp = new XMLHttpRequest();

xhttp.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
        mostrarDireccion(this.responseText);
    }
};

xhttp.open("GET", "http://127.0.0.1:5501/json/formularioCampos.json", true);
xhttp.send();
}

function mostrarDireccion(jsonDoc){

var objetoJsonDireccion = JSON.parse(jsonDoc);

console.log(objetoJsonDireccion);

arrayDireccion = objetoJsonDireccion.formuDireccion;
arrayEspacioDireccion = objetoJsonDireccion.espacio;

for(let formuDireccion of arrayDireccion){

    leftDireccion = document.createElement(formuDireccion.elementleft);
    textoleftDireccion = document.createTextNode(formuDireccion.title);
    leftDireccion.appendChild(textoleftDireccion);
    campoDireccion = document.createElement(formuDireccion.elementinput);

    leftDireccion.setAttribute("class", formuDireccion.classObligado);
    campoDireccion.id = formuDireccion.direccionid;

    document.getElementById("idDireccion").appendChild(leftDireccion);
    document.getElementById("idDireccion").appendChild(campoDireccion);
}

for(let espacio of arrayEspacioDireccion){

    br3 = document.createElement(espacio.espacio);
    br4 = document.createElement(espacio.espacio);

    document.getElementById("idDireccion").appendChild(br3);
    document.getElementById("idDireccion").appendChild(br4);
}
}