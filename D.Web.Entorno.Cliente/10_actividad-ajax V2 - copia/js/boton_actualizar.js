//FUNCIÓN BOTÓN ACTUALIZAR

function inicioActualizar(){
    var divActualizar = document.createElement("div");
    divActualizar.id = "butActualizar";

    document.body.appendChild(divActualizar);

    buttonActualizar();
}
function  buttonActualizar(){

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
             mostrarActualizar(this.responseText);
        }
    };

    xhttp.open("GET", "http://127.0.0.1:5501/json/botones.json", true);
    xhttp.send();

}

function mostrarActualizar(jsonDoc){

    var objetoJSONActualizar = JSON.parse(jsonDoc);

    console.log(objetoJSONActualizar);

    arrayActualizar = objetoJSONActualizar.actualizarBoton;

    for(let actualizarBoton of arrayActualizar){

        botonActualizar = document.createElement(actualizarBoton.elementbutton);
        textbotonActualizar = document.createTextNode(actualizarBoton.title);
        botonActualizar.appendChild(textbotonActualizar);
        botonActualizar.addEventListener("click", actualizar);
        document.getElementById("butActualizar").appendChild(botonActualizar);
      
    }
  }
