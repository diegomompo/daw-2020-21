// FUNCION TEXTO INGREDIENTES
function inicioExtra(){
    var divExtra = document.createElement("div");
    divExtra.id = "extra";

    document.getElementById("reiniciar").appendChild(divExtra);

    mostrarIngredientes();

}
function mostrarIngredientes(){

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        respuestaExtra(this.responseText);
    }
  };
  xhttp.open("GET", "http://127.0.0.1:5501/json/ingredientes.json", true);
  xhttp.send();
}

function respuestaExtra(jsonDoc){
    var objetoJsonExtra = JSON.parse(jsonDoc);
    console.log(objetoJsonExtra);

    var arrayPExtra = objetoJsonExtra.extra.elementpExtra;
    var arrayExtra = objetoJsonExtra.extra.ingredientesExt;
    var arraybrExtra = objetoJsonExtra.extra.espacioExtra;

    for(let elementpExtra of arrayPExtra){

    pExtra = document.createElement(elementpExtra.elementP);
    textpExtra = document.createTextNode(elementpExtra.titleExtra);
    pExtra.appendChild(textpExtra);
    pExtra.setAttribute("class", elementpExtra.classObligado);

    document.getElementById("extra").appendChild(pExtra);

    }

    for(let ingredientesExt of arrayExtra){
        divExtra = document.createElement(ingredientesExt.elementDiv);
        divExtra.id = ingredientesExt.idDiv;

        leftExtraNombre = document.createElement(ingredientesExt.elementLeft);
        textoExtraNombre = document.createTextNode(ingredientesExt.titleNombre);
        leftExtraNombre.appendChild(textoExtraNombre);

        inputExtra = document.createElement(ingredientesExt.elementInput);
        inputExtra.setAttribute("type", ingredientesExt.type);
        inputExtra.setAttribute("name", ingredientesExt.name);
        inputExtra.setAttribute("value", ingredientesExt.value);
        
        leftExtraPrecio = document.createElement(ingredientesExt.elementLeft);
        textoExtraPrecio = document.createTextNode(ingredientesExt.titlePrecio);
        leftExtraPrecio.appendChild(textoExtraPrecio)

        document.getElementById("extra").appendChild(divExtra);
        document.getElementById("extra").appendChild(leftExtraNombre);
        document.getElementById("extra").appendChild(inputExtra);
        document.getElementById("extra").appendChild(leftExtraPrecio);

    }

    for(let espacioExtra of arraybrExtra){
        espExtra = document.createElement(espacioExtra.espacio);
        espExtra1 = document.createElement(espacioExtra.espacio);
        document.getElementById("extra").appendChild(espExtra);
        document.getElementById("extra").appendChild(espExtra1);
    }

}