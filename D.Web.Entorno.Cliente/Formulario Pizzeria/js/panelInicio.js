function PanelInicioPedido(){
    var divIni = $("<div>");
    $("body").append(divIni);

    panelInicio();
}
function panelInicio(){
    $.ajax({
        'type' : 'GET',
        'url' : "http://127.0.0.1:5500/json/inicioPizza.json",
        'async' : true,
        }
    ).done(mostrarPanelInicio)
}
function mostrarPanelInicio(objetoJsonPanelInicio){

    let arrayPanelInicio = objetoJsonPanelInicio.inicioPanelPizzeria;
    let arrayTituloPanelInicio = objetoJsonPanelInicio.estadoPizzeria;
    let arrayCuerpoInicio = objetoJsonPanelInicio.cuerpoInicioPizzeria;
    let arrayTamanoPizza = objetoJsonPanelInicio.tamanoPizza;
    let arrayIngExtra = objetoJsonPanelInicio.ingExtra;

    $.each(arrayPanelInicio, function(i, inicioPanelPizzeria){
        $.each(arrayTituloPanelInicio, function(i, estadoPizzeria){
            $.each(arrayCuerpoInicio, function(i, cuerpoInicioPizzeria){
                $.each(arrayTamanoPizza, function(i, tamanoPizza){
                    $.each(arrayIngExtra, function(i, ingExtra){

                        // CUERPO DE DEL PANEL

                        //TITULO

                        let h3TituloPanel = $(estadoPizzeria.elementh3);
                        h3TituloPanel.attr("class", inicioPanelPizzeria.classTextCenter);
                           h3TituloPanel.text(estadoPizzeria.title);

                        //-------------------------------------------------------------------------------------
                
                        //PRIMERA IMAGEN
                
                        let img1 = $(cuerpoInicioPizzeria.elementimg);
                        img1.attr("id", cuerpoInicioPizzeria.idimg1);
                        img1.attr("src", cuerpoInicioPizzeria.srcimg1);
                        img1.attr("class", cuerpoInicioPizzeria.classimg1);

                        let formInicio = $(cuerpoInicioPizzeria.elementform);
                        formInicio.attr("action", cuerpoInicioPizzeria.formaction);
                        formInicio.attr("method", cuerpoInicioPizzeria.formethod);
                         formInicio.attr("id", cuerpoInicioPizzeria.idform);

                        //---------------------------------------------------------------------------------------------

                        //TAMAÑO PIZZA

                        let h3Tamano = $(estadoPizzeria.elementh3);
                        h3Tamano.text(cuerpoInicioPizzeria.titleTamano)

                        let selectorTamano = $(cuerpoInicioPizzeria.elementselect);
                        selectorTamano.attr("name", cuerpoInicioPizzeria.nameselect);

                        let opcionPequena = $(cuerpoInicioPizzeria.elementoption);
                        opcionPequena.attr("value", tamanoPizza.valuePequena);
                        opcionPequena.text(tamanoPizza.titlePequena);

                        let opcionMediana = $(cuerpoInicioPizzeria.elementoption);
                        opcionMediana.attr("value", tamanoPizza.valueMediana);
                        opcionMediana.text(tamanoPizza.titleMediana);

                        let opcionGrande = $(cuerpoInicioPizzeria.elementoption);
                        opcionGrande.attr("value",tamanoPizza.valueGrande);
                        opcionGrande.text(tamanoPizza.titleGrande);
                
                        //--------------------------------------------------------------------------------------------------------------------------

                        let espacio1 = $(cuerpoInicioPizzeria.elementbr);
                        let espacio2 = $(cuerpoInicioPizzeria.elementbr);

                        //----------------------------------------------------------------------------------------------------------------

                        //INGREDIENTES

                        let h3Extra = $(estadoPizzeria.elementh3);
                        h3Extra.text(cuerpoInicioPizzeria.titleExtra);
                        
                        let divCheckbox = $(inicioPanelPizzeria.elementdiv);
                        divCheckbox.attr("id", cuerpoInicioPizzeria.id1Div);

                        //------------------------------------------------------------------------------------------------------

                        //PRECIO TOTAL

                        let centerPrecio = $(inicioPanelPizzeria.elementcenter);
                        let pPrecio = $(cuerpoInicioPizzeria.elementp);
                        pPrecio.attr("id", cuerpoInicioPizzeria.idprecio);
                        pPrecio.text(cuerpoInicioPizzeria.titlePrecio);
                        let spanPrecio = $(cuerpoInicioPizzeria.elementspan);
                        spanPrecio.text(cuerpoInicioPizzeria.titlespan)

                        pPrecio.click(sumartodo);



                        //--------------------------------------------------------------------------------------------------------------

                        //BOTON

                        let inputSubmit = $(cuerpoInicioPizzeria.elementinput)
                        inputSubmit.attr("type", cuerpoInicioPizzeria.typesubmit);
                        inputSubmit.attr("value", cuerpoInicioPizzeria.valuesubmit);
                        inputSubmit.attr("name", cuerpoInicioPizzeria.namesubmit)

                        inputSubmit.click(validacion);

                        //------------------------------------------------------------------------------------------------------------------

                        let elementDivImg2 = $(inicioPanelPizzeria.elementdiv);
                        elementDivImg2.attr("class", inicioPanelPizzeria.classTextCenter);
                        let img2 = $(cuerpoInicioPizzeria.elementimg)
                        img2.attr("src", cuerpoInicioPizzeria.srcImg2);
                         img2.attr("class", cuerpoInicioPizzeria.classImg2);


                        //RESULTADO

                        $("#id_panelHeading").append(h3TituloPanel);

                        $("#id_panelBody").append(img1);

                        $("#id_panelBody").append(formInicio);

                        $(formInicio).append(h3Tamano);
                        $(formInicio).append(selectorTamano);
                        $(selectorTamano).append(opcionPequena);
                        $(selectorTamano).append(opcionMediana);
                        $(selectorTamano).append(opcionGrande);

                        $(formInicio).append(espacio1);
                        $(formInicio).append(espacio2);

                        $(formInicio).append(h3Extra);
                        $(formInicio).append(divCheckbox);

                        $(formInicio).append(centerPrecio);
                        $(centerPrecio).append(pPrecio);
                        $(pPrecio).append(spanPrecio);

                        $(formInicio).append(inputSubmit);

                         $("#id_panelBody").append(elementDivImg2);
                        $(elementDivImg2).append(img2);

                        extra();
                        
                    })
                })
            })
        })
    })
}