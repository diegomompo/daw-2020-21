window.onload = function(){

    $(document).ready(inicioFormu);
}

function inicioFormu(){
    let divFormu = $("<div>");
    divFormu.attr("id", "formu");

    $("body").append(divFormu);

    inicioFormulario();
}
function inicioFormulario(){
    $.ajax({
        'type': 'GET',
        'url': "http://127.0.0.1:5501/json/formularioPizzeria.json",
        "async": true
    }).done(mostrarInicioFormu)
}

function mostrarInicioFormu(objetoJSONFormu){

    arrayHead = objetoJSONFormu.headFormu;
    arrayFormu = objetoJSONFormu.formuPizza;

    // HEAD DE LA PÁGINA 

    $.each(arrayHead, function(i, headFormu){
            window.open("barra_preparando.html" , "ventana1" , "width=1920,height=150,top=750,scrollbars=NO")

            let elementMeta = $(headFormu.elementmeta);
            elementMeta.attr("charset", headFormu.charsetutf);

            let elementMeta2 = $(headFormu.elementmeta);
            elementMeta2.attr("name", headFormu.namemeta);
            elementMeta2.attr("content", headFormu.contentmeta);

            let elementTitle = $(headFormu.elementtitle);
            elementTitle.text(headFormu.title);


            let elementLinkEstilos = $(headFormu.elementlink);
            elementLinkEstilos.attr("rel", headFormu.relestilos);
            elementLinkEstilos.attr("href", headFormu.hrefestilos);

           let elementLinkFuente = $(headFormu.elementlink);
            elementLinkFuente.attr("rel", headFormu.relestilos);
            elementLinkFuente.attr("href", headFormu.hrefFuente);
        

            $("head").append(elementMeta);
            $("head").append(elementMeta2);
            $("head").append(elementTitle);
            $("head").append(elementLinkEstilos);
            $("head").append(elementLinkFuente);
    })
    //-------------------------------------------------------------------------------------------------------------------------------
    $.each(arrayFormu, function(i, formuPizza){

        //FORMULARIO

            let elementForm = $(formuPizza.elementform);
            elementForm.attr("action", formuPizza.formAction);
            elementForm.attr("method", formuPizza.formMethod);
            elementForm.attr("id", formuPizza.idForm);

            let elementDivFormu = $(formuPizza.elementdiv);
            elementDivFormu.attr("class", formuPizza.classForm);

            let elementh1Titulo = $(formuPizza.elementh1);
            elementh1Titulo.text(formuPizza.titleh1);

        //-------------------------------------------------------------------------------------------------------

            // CAMPO NOMBRE 

            let elementDivNombre = $(formuPizza.elementdiv);
            elementDivNombre.attr("class", formuPizza.classFormu);

            let inputNombre = $(formuPizza.elementinput);
            inputNombre.attr("required", true);
            inputNombre.attr("type", formuPizza.typeText);
            inputNombre.attr("id", formuPizza.idNombre);
            
            let spanNombre = $(formuPizza.elementspan);
            spanNombre.attr("class", formuPizza.classspan);

            let labelNombre = $(formuPizza.elementlabel);
            labelNombre.text(formuPizza.titleNombre);

        //-------------------------------------------------------------------------------------------------------

            // CAMPO DIRECCIÓN

            let elementDivDireccion = $(formuPizza.elementdiv);
            elementDivDireccion.attr("class", formuPizza.classFormu);

            let inputDireccion = $(formuPizza.elementinput);
            inputDireccion.attr("required", true);
            inputDireccion.attr("type", formuPizza.typeText);
            inputDireccion.attr("id", formuPizza.idDireccion);
            
            let spanDireccion = $(formuPizza.elementspan);
            spanDireccion.attr("class", formuPizza.classspan);

            let labelDireccion = $(formuPizza.elementlabel);
            labelDireccion.text(formuPizza.titleDireccion);

        //-------------------------------------------------------------------------------------------------------

            // CAMPO TELEFONO 

           let elementDivTelefono= $(formuPizza.elementdiv);
            elementDivTelefono.attr("class", formuPizza.classFormu);

            let inputTelefono = $(formuPizza.elementinput);
            inputTelefono.attr("required", true);
            inputTelefono.attr("type", formuPizza.typeTel);
            inputTelefono.attr("id", formuPizza.idTel);
            
            let spanTelefono = $(formuPizza.elementspan);
            spanTelefono.attr("class", formuPizza.classspan);

            let labelTelefono = $(formuPizza.elementlabel);
            labelTelefono.text(formuPizza.titleTel);

        //-------------------------------------------------------------------------------------------------------

            // CAMPO HORA

            let elementDivTime= $(formuPizza.elementdiv);
            elementDivTime.attr("class", formuPizza.classFormu);

            let inputTime = $(formuPizza.elementinput);
            inputTime.attr("required", true);
            inputTime.attr("type", formuPizza.typeTime);
            inputTime.attr("id", formuPizza.idTime);
            
            let spanTime = $(formuPizza.elementspan);
            spanTime.attr("class", formuPizza.classspan);

            let labelTime = $(formuPizza.elementlabel);
            labelTime.text(formuPizza.titleTime);

        //-------------------------------------------------------------------------------------------------------

            // CAMPO EMAIL

            let elementDivEmail = $(formuPizza.elementdiv);
            elementDivEmail.attr("class", formuPizza.classFormu);

            let inputEmail = $(formuPizza.elementinput);
            inputEmail.attr("required", true);
            inputEmail.attr("type", formuPizza.typeEmail);
            inputEmail.attr("id", formuPizza.idEmail);
            
            let spanEmail = $(formuPizza.elementspan);
            spanEmail.attr("class", formuPizza.classspan);

            let labelEmail = $(formuPizza.elementlabel);
            labelEmail.text(formuPizza.titleEmail);

        //-------------------------------------------------------------------------------------------------------

            // CAMPO CONTRASEÑA

            let elementDivContraseña = $(formuPizza.elementdiv);
            elementDivContraseña.attr("class", formuPizza.classFormu);

            let inputContraseña = $(formuPizza.elementinput);
            inputContraseña.attr("required", true);
            inputContraseña.attr("type", formuPizza.typeContra);
            inputContraseña.attr("id", formuPizza.idContra);
            
            let spanContraseña = $(formuPizza.elementspan);
            spanContraseña.attr("class", formuPizza.classspan);

            let labelContraseña = $(formuPizza.elementlabel);
            labelContraseña.text(formuPizza.titleContra);

        //-----------------------------------------------------------------------------------------------------

            //BOTON ENVIAR

            let elementButton = $(formuPizza.elementbutton);
            elementButton.attr("type", "submit");
            elementButton.text(formuPizza.titleButton);
            elementButton.click(validarForm);
        //-----------------------------------------------------------------------

            //BOTON NUEVO PEDIDO

            let elementP = $(formuPizza.elementp);
            elementP.attr("class", formuPizza.classP);
            elementP.attr("id", formuPizza.idP);

        //----------------------------------------------------------------------------------------------------

            // RESULTADO

            $("#formu").append(elementForm);
            $(elementForm).append(elementDivFormu);
            $(elementDivFormu).append(elementh1Titulo);

            $(elementDivFormu).append(elementDivNombre);
            $(elementDivNombre).append(inputNombre);
            $(elementDivNombre).append(spanNombre);
            $(elementDivNombre).append(labelNombre);

            $(elementDivFormu).append(elementDivDireccion);
            $(elementDivDireccion).append(inputDireccion);
            $(elementDivDireccion).append(spanDireccion);
            $(elementDivDireccion).append(labelDireccion);
            
            $(elementDivFormu).append(elementDivTelefono);
            $(elementDivTelefono).append(inputTelefono);
            $(elementDivTelefono).append(spanTelefono);
            $(elementDivTelefono).append(labelTelefono);

            $(elementDivFormu).append(elementDivTime);
            $(elementDivTime).append(inputTime);
            $(elementDivTime).append(spanTime);
            $(elementDivTime).append(labelTime);

            $(elementDivFormu).append(elementDivEmail);
            $(elementDivEmail).append(inputEmail);
            $(elementDivEmail).append(spanEmail);
            $(elementDivEmail).append(labelEmail);

            $(elementDivFormu).append(elementDivContraseña);
            $(elementDivContraseña).append(inputContraseña);
            $(elementDivContraseña).append(spanContraseña);
            $(elementDivContraseña).append(labelContraseña);

            $(elementDivFormu).append(elementButton);

            $(elementDivFormu).append(elementP);
    })
}
