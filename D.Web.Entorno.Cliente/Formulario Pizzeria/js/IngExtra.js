function extra(){
    var divIni = $("<div>");
    $("body").append(divIni);

    ingExtra();
}
function ingExtra(){
    $.ajax({
        'type' : 'GET',
        'url' : "http://127.0.0.1:5500/json/inicioPizza.json",
        'async' : true,
        }
    ).done(mostrarIngExtra)
}
function mostrarIngExtra(objetoJsonPanelInicio){

    let arrayPanelInicio = objetoJsonPanelInicio.inicioPanelPizzeria;
    let arrayTituloPanelInicio = objetoJsonPanelInicio.estadoPizzeria;
    let arrayCuerpoInicio = objetoJsonPanelInicio.cuerpoInicioPizzeria;
    let arrayTamanoPizza = objetoJsonPanelInicio.tamanoPizza;
    let arrayIngExtra = objetoJsonPanelInicio.ingExtra;

    $.each(arrayPanelInicio, function(i, inicioPanelPizzeria){
        $.each(arrayTituloPanelInicio, function(i, estadoPizzeria){
            $.each(arrayCuerpoInicio, function(i, cuerpoInicioPizzeria){
                $.each(arrayTamanoPizza, function(i, tamanoPizza){
                    $.each(arrayIngExtra, function(i, ingExtra){

                        //PRIMER INGREDIENTE

                        let divIng1 = $(inicioPanelPizzeria.elementdiv);
                        divIng1.attr("id", cuerpoInicioPizzeria.id2Div);

                        let pIng1 = $(cuerpoInicioPizzeria.elementp);
                        pIng1.text(ingExtra.titleing1);

                        let inputIng1 = $(cuerpoInicioPizzeria.elementinput);
                        inputIng1.attr("class", ingExtra.classing1);
                        inputIng1.attr("type", cuerpoInicioPizzeria.typeExtra);
                        inputIng1.attr("id", ingExtra.iding1);
                        inputIng1.attr("name", ingExtra.nameing1);
                        inputIng1.attr("value", ingExtra.valueing1);

                        let labelIng1 = $(cuerpoInicioPizzeria.elementlabel);
                        labelIng1.attr("class", ingExtra.classing1);
                        labelIng1.attr("for", ingExtra.foring1);

                        let pValueIng1 = $(cuerpoInicioPizzeria.elementp);
                        pValueIng1.text(ingExtra.valueing1);

                        //----------------------------------------------------------

                        //SEGUNDO INGREDIENTE

                        let divIng2 = $(inicioPanelPizzeria.elementdiv);
                        divIng2.attr("id", cuerpoInicioPizzeria.id2Div);

                        let pIng2 = $(cuerpoInicioPizzeria.elementp);
                        pIng2.text(ingExtra.titleing2);

                        let inputIng2 = $(cuerpoInicioPizzeria.elementinput);
                        inputIng2.attr("class", ingExtra.classing2);
                        inputIng2.attr("type", cuerpoInicioPizzeria.typeExtra);
                        inputIng2.attr("id", ingExtra.iding2);
                        inputIng2.attr("name", ingExtra.nameing2);
                        inputIng2.attr("value", ingExtra.valueing2);

                        let labelIng2 = $(cuerpoInicioPizzeria.elementlabel);
                        labelIng2.attr("class", ingExtra.classing2);
                        labelIng2.attr("for", ingExtra.foring2);

                        let pValueIng2 = $(cuerpoInicioPizzeria.elementp);
                        pValueIng2.text(ingExtra.valueing2);

                        //----------------------------------------------------------

                        //TERCER INGREDIENTE

                       let divIng3 = $(inicioPanelPizzeria.elementdiv);
                        divIng3.attr("id", cuerpoInicioPizzeria.id2Div);

                        let pIng3 = $(cuerpoInicioPizzeria.elementp);
                        pIng3.text(ingExtra.titleing3);

                        let inputIng3 = $(cuerpoInicioPizzeria.elementinput);
                        inputIng3.attr("class", ingExtra.classing3);
                        inputIng3.attr("type", cuerpoInicioPizzeria.typeExtra);
                        inputIng3.attr("id", ingExtra.iding3);
                        inputIng3.attr("name", ingExtra.nameing3);
                        inputIng3.attr("value", ingExtra.valueing3);

                        let labelIng3 = $(cuerpoInicioPizzeria.elementlabel);
                        labelIng3.attr("class", ingExtra.classing3);
                        labelIng3.attr("for", ingExtra.foring3);

                        let pValueIng3 = $(cuerpoInicioPizzeria.elementp);
                        pValueIng3.text(ingExtra.valueing3);

                        //----------------------------------------------------------

                        //CUARTO INGREDIENTE

                        let divIng4 = $(inicioPanelPizzeria.elementdiv);
                        divIng4.attr("id", cuerpoInicioPizzeria.id2Div);

                        let pIng4 = $(cuerpoInicioPizzeria.elementp);
                        pIng4.text(ingExtra.titleing4);

                        let inputIng4 = $(cuerpoInicioPizzeria.elementinput);
                        inputIng4.attr("class", ingExtra.classing4);
                        inputIng4.attr("type", cuerpoInicioPizzeria.typeExtra);
                        inputIng4.attr("id", ingExtra.iding4);
                        inputIng4.attr("name", ingExtra.nameing4);
                        inputIng4.attr("value", ingExtra.valueIng4);

                        let labelIng4 = $(cuerpoInicioPizzeria.elementlabel);
                        labelIng4.attr("class", ingExtra.classing4);
                        labelIng4.attr("for", ingExtra.foring4);

                        let pValueIng4 = $(cuerpoInicioPizzeria.elementp);
                        pValueIng4.text(ingExtra.valueing4);

                        //----------------------------------------------------------

                        //QUINTO INGREDIENTE

                        let divIng5 = $(inicioPanelPizzeria.elementdiv);
                        divIng5.attr("id", cuerpoInicioPizzeria.id2Div);

                        let pIng5 = $(cuerpoInicioPizzeria.elementp);
                        pIng5.text(ingExtra.titleing5);

                        let inputIng5 = $(cuerpoInicioPizzeria.elementinput);
                        inputIng5.attr("class", ingExtra.classing5);
                        inputIng5.attr("type", cuerpoInicioPizzeria.typeExtra);
                        inputIng5.attr("id", ingExtra.iding5);
                        inputIng5.attr("name", ingExtra.nameing5);
                        inputIng5.attr("value", ingExtra.valueing5);

                        let labelIng5 = $(cuerpoInicioPizzeria.elementlabel);
                        labelIng5.attr("class", ingExtra.classing5);
                        labelIng5.attr("for", ingExtra.foring5);

                        let pValueIng5 = $(cuerpoInicioPizzeria.elementp);
                        pValueIng5.text(ingExtra.valueing5);

                        //----------------------------------------------------------

                        //Resultado

                        $("#checkbox_container").append(divIng1);
                        $(divIng1).append(pIng1);
                        $(divIng1).append(inputIng1);
                        $(divIng1).append(labelIng1);
                        $(divIng1).append(pValueIng1);

                        $("#checkbox_container").append(divIng2);
                        $(divIng2).append(pIng2);
                        $(divIng2).append(inputIng2);
                        $(divIng2).append(labelIng2);
                        $(divIng2).append(pValueIng2);

                        $("#checkbox_container").append(divIng3);
                        $(divIng3).append(pIng3);
                        $(divIng3).append(inputIng3);
                        $(divIng3).append(labelIng3);
                        $(divIng3).append(pValueIng3);

                        $("#checkbox_container").append(divIng4);
                        $(divIng4).append(pIng4);
                        $(divIng4).append(inputIng4);
                        $(divIng4).append(labelIng4);
                        $(divIng4).append(pValueIng4);

                        $("#checkbox_container").append(divIng5);
                        $(divIng5).append(pIng5);
                        $(divIng5).append(inputIng5);
                        $(divIng5).append(labelIng5);
                        $(divIng5).append(pValueIng5);
                    })
                })
            })
        })
    })
}