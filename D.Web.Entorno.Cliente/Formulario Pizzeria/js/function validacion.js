/**var ing=PedidodePizza.getElementById("checkbox_container")

function validacion(){
    if(!ingrediente1.checked+ !ingrediente2.checked+ !ingrediente3.checked+ !ingrediente4.checked+ !ingrediente5.checked)
        alert ("Tiene que elegir un ingrediente")
        return false;
}
Es así como lo validaría si lo hiciera sin JQuery*/

function validacion() {
         if($("input[type=checkbox]:checked").length != 0){
            //OK
         }else{
           alert('Tienes hambre ¿verdad? Pero primero debes seleccionar al menos un ingrediente');
           return false;
         }
         /**Sin embargo como he decidido complicarme la vida con las JQuery, he creado esta validación que
          * genera a través de un simple condicional que se verifique si el usuario selecciona algún ingrediente.
          * Si el checkbox es seleccionado, lo cual conseguimos con el atributo checked y, su longitud (length) no
          * es igual a 0, damos el OK; si no es así retornamos un mensaje al usuario que le alerta de que debe seleccionar
          * al menos un ingrediente
          */
}     
         