// FUNCION PARA MOSTRAR EL TEXTO DE LA BARRA DE ENCENDER EL HORNO

function barraEncendiendo(){

    var header5 = $("<h1>");
    header5.text("Encendiendo el horno");
    $("body").append(header5);
    cuerpoBarra()   
    move();
}

//-------------------------------------------------------------------------------------------------

// FUNCION PARA MOSTRAR EL TEXTO DE LA BARRA de VALIDAR FORMULARIO

function barraValidando(){

    var header5 = $("<h1>");
    header5.text("Validando del formulario");
    $("body").append(header5);
    cuerpoBarra()   
    move();
}
//-------------------------------------------------------------------------------------------------

// FUNCION PARA MOSTRAR EL TEXTO DE LA BARRA de PREPARAR EL FORMULARIO

function barraPreparando(){

    var header5 = $("<h1>");
    header5.text("Preparando del formulario");
    $("body").append(header5);
    cuerpoBarra()   
    move();
}

//-------------------------------------------------------------------------------------------------

// FUNCION QUE DEFINE EL CUERPO DE LA BARRA Y EK BOTON DE CERRAR LA BARRA

function cuerpoBarra(){
    var fondo = $("<div>");
    fondo.attr("class", "estiloFondo");
    fondo.attr("id", "eFondo");

    var eBarra = $("<div>");
    eBarra.attr("class", "estiloBarra");
    eBarra.attr("id", "barra");

    var ePorcentaje = $("<div>");
    ePorcentaje.attr("class", "estiloPorcentaje");
    ePorcentaje.attr("id", "porcentaje");
    ePorcentaje.text("0%");

    var botonCerrar = $("<button>");

    botonCerrar.attr("type", "submit");
    botonCerrar.attr("class", "btn btn-warning");
    botonCerrar.text("Cerrar");
    botonCerrar.click(cerrar);

   $("body").append(fondo);
    $("#eFondo").append(eBarra);
    $("#barra").append(ePorcentaje);
   
    $("body").append(botonCerrar);
}