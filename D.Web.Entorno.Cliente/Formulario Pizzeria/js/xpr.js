var nombre = document.getElementById("nombre")
var telefono = document.getElementById("tel")
var email = document.getElementById("email")
var pass = document.getElementById("password")
var form = document.getElementById("formu")
var parrafo = document.getElementById("warnings")
/**declaramos las variables y cogemos los id de elementos definidos en el index.html */
function validarForm(){
     window.open("barra_validando.html" , "ventana1" , "width=1920,height=150,top=750,scrollbars=NO")
    /**impedimos con la función preventDefault que el explorador ejecute la acción determinada */
    let warnings = ""
    let entrar = false
    /**esta variable es el valor que va a tomar por defecto para ir validando */
    let patronNombre = /^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/;
    let patronTel =/^[9|6]{1}([\d]{2}[-]*){3}[\d]{2}$/;
    let patronEmail = /^[\w]+@{1}[\w]+\.+[a-z]{2,3}$/;
    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/
    "p[idP=warnings]".innerHTML = ""
    /**y aquí generamos las variables con carácter local para que "jueguen" en este ámbito de bloque 
     * A partir de aquí vamos a generar todas las validaciones usando los maravillosos if o condicionales
     * simples
    */
    if(!patronNombre.test($("#nombre").val())){
        /**Con el método test comprobamos alguna coincidencia de la cadena. Si los valores de la variable
         * local asignada para nombre no coinciden, se genera una alerta al usuario. 
        */
        warnings += `El nombre no es valido. Debe comenzar por mayúsculas.`
        entrar = true
    }
    if(!patronTel.test($("#tel").val())){
        warnings += `El telefono no es valido. Debe comenzar por 6 o 9 y contener 9 dígitos.`
        entrar = true
    }
    if(!regexEmail.test($("#email").val())){
        warnings += `El email no es valido.`
        entrar = true
    }
    if($("#password").val().length < 8){
        warnings += "La contraseña no es valida. Debe introducir al menos 8 dígitos."
        entrar = true
        /**evaluamos teniendo como base la longitud que hemos estimado para la contraseña
         * y devolvemos al usuario el mensaje correspondiente
         */
    }

    if(entrar){
       $("#warnings").text(warnings)
    }else{
        $("#warnings").text("¡Enciendan horno!")
    }
    /**y finalizamos con el ultimo condicional donde definimos simplemente que si al entrar no hay errores
     * en las validaciones se genere un texto, en nuestro caso ¡Enciendan horno! y si no remita los warnings
     * que es la variable que hemos definido para generar las validaciones erróneas
     */
}