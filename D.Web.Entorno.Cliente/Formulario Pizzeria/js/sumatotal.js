function sumartodo() {

    var size = 0;
    $('select').change(function () {
        size = ($(this).val());
        addValue(size);
    });
    addValue(size);
    function addValue(size) {
        var total = 0;
        function updateTextArea() {
            var allVals = [];
            $('#checkbox_container :checked').each(function () {
                allVals.push($(this).val());
            });
            if (size === 0) {
                size = 5;
            }
            total = parseFloat(size) + parseFloat(eval(allVals.join("+")));
            /**Gracias a la función parseFloat cuyo valor es la variable creada "size"
             * y los ingredientes seleccionados en el checkbox, se realiza la suma y saca el total. Si no 
             * añadieramos valor a esta función, nos devolvería el retorno NAN ¿y qué se encarga de esto?
             */
            if (isNaN(total)) {
                /**la función isNan que toma el total definido, que es el parámetro y lo transforma en un número */
                if (size === 0) {
                    total = 5;
                }
                else {
                    total = size;
                }
                /**usamos un condicional if else */
            }
            $('p#total span').html(total + "€");
        }   /**mostramos el final de la función */
        $(function () {
            $('#checkbox_container input').click(updateTextArea);
            updateTextArea();
        });
    }
}

           