function cuerpo(){
    textoInicio();
    obligado();
    formulario();
}

function textoInicio(){
    var header1 = document.createElement("h1")
        var textheader1 = document.createTextNode("Actividad: DOM y Formularios");
        header1.appendChild(textheader1);
    document.body.appendChild(header1);

}
function obligado(){
    var textObligado = document.createElement("p");
        textObligado.innerHTML = "Los textos en rojo es obligatorio completarlos";
    document.body.appendChild(textObligado);
}
function formulario(){

    var formu = document.createElement("form");
    formu.id = "reiniciar";

    var leftDNI = document.createElement("left");
    leftDNI.innerHTML = "DNI: ";
    var campoDNI = document.createElement("input");

    leftDNI.setAttribute("class", "obligatorio");
    campoDNI.setAttribute("class", "cuadro");
    campoDNI.id = "textoDNI";

    var br1 = document.createElement("br");
    var br2 = document.createElement("br");

    var leftNombre = document.createElement("left");
    leftNombre.innerHTML = "Nombre: "
    var campoNombre = document.createElement("input");

    leftNombre.setAttribute("class", "obligatorio");
    campoNombre.setAttribute("class", "cuadro");
    campoNombre.id = "textoNombre";


    var br3 = document.createElement("br");
    var br4 = document.createElement("br");

    var leftPApellido = document.createElement("left");
    leftPApellido.innerHTML = "Primer Apellido: "
    var campoPApellido = document.createElement("input");

    leftPApellido.setAttribute("class", "obligatorio");
    campoPApellido.setAttribute("class", "cuadro");
    campoPApellido.id = "textoApellido1";

    var br5 = document.createElement("br");
    var br6 = document.createElement("br");

    var leftSApellido= document.createElement("left");
    leftSApellido.innerHTML = "Segundo Apellido: "
    var campoSApellido = document.createElement("input");

   campoSApellido.setAttribute("class", "cuadro");
    campoSApellido.id = "textoApellido2";


    var br7 = document.createElement("br");
    var br8 = document.createElement("br");

    var PreguntaSocio = document.createElement("p");
    PreguntaSocio.innerHTML = "¿Desea hacerse socio?";

    PreguntaSocio.setAttribute("class", "obligatorio");

    
    var leftSocioSi= document.createElement("left");
    leftSocioSi.innerHTML = "Si ";

    var inputSocioSi = document.createElement("input");
    inputSocioSi.setAttribute("type", "radio");
    inputSocioSi.setAttribute("value", "si");
    inputSocioSi.setAttribute("name", "pregunta");
    inputSocioSi.id = "pregunta_si";

    var br9 = document.createElement("br");

    var leftSocioNo= document.createElement("left");
    leftSocioNo.innerHTML = "No ";

    var inputSocioNo = document.createElement("input");;
    inputSocioNo.setAttribute("type", "radio");
    inputSocioNo.setAttribute("value", "no");
    inputSocioNo.setAttribute("name", "pregunta");;
    inputSocioNo.id = "pregunta_no";

    var br10 = document.createElement("br");

    var leftSocioNC= document.createElement("left");
    leftSocioNC.innerHTML = "NC ";

    var inputSocioNC = document.createElement("input");
    inputSocioNC.setAttribute("type", "radio");
    inputSocioNC.setAttribute("value", "NC");
    inputSocioNC.setAttribute("name", "pregunta");;
    inputSocioNC.id = "pregunta_nc";

    var br11 = document.createElement("br");
    var br12 = document.createElement("br");

    var leftTerminos= document.createElement("left");
    leftTerminos.innerHTML = "He leído y aceptado los términos y condiciones";

    var inputTerminos = document.createElement("input");
    inputTerminos.setAttribute("type", "checkbox");

    leftTerminos.setAttribute("class", "obligatorio");

    inputTerminos.id = "condiciones";

    var br13 = document.createElement("br");

    var leftPrivacidad= document.createElement("left");
    leftPrivacidad.innerHTML = "He leído y aceptado la política de privacidad ";

    var inputPrivacidad = document.createElement("input");
    inputPrivacidad.setAttribute("type", "checkbox");

    leftPrivacidad.setAttribute("class", "obligatorio");

    inputPrivacidad.id = "privacidad";

    var br14 = document.createElement("br");

    var leftNotificaciones= document.createElement("left");
    leftNotificaciones.innerHTML = "Deseo recibir notificaciones sobre el sitio web";

    var inputNotificaciones = document.createElement("input");
    inputNotificaciones.setAttribute("type", "checkbox");

    leftNotificaciones.setAttribute("class", "obligatorio");

    inputNotificaciones.id = "notificaciones";

    var br15 = document.createElement("br");
    var br16 = document.createElement("br");

    var botonEnviar = document.createElement("button"); // Add onclick function to element

    botonEnviar.onclick = recogiendo


    botonEnviar.innerHTML = "Enviar";

    var botonReiniciar = document.createElement("button");

    botonReiniciar.onclick = reinicio

    botonReiniciar.innerHTML = "Limpiar Campos";

    //document.body.appendChild(formulario);
    document.body.appendChild(formu);
    document.getElementById("reiniciar").appendChild(leftDNI);
    document.getElementById("reiniciar").appendChild(campoDNI);
    document.getElementById("reiniciar").appendChild(br1)
    document.getElementById("reiniciar").appendChild(br2);
    document.getElementById("reiniciar").appendChild(leftNombre);
    document.getElementById("reiniciar").appendChild(campoNombre);
    document.getElementById("reiniciar").appendChild(br3);
    document.getElementById("reiniciar").appendChild(br4);
    document.getElementById("reiniciar").appendChild(leftPApellido);
    document.getElementById("reiniciar").appendChild(campoPApellido);
    document.getElementById("reiniciar").appendChild(br5);
    document.getElementById("reiniciar").appendChild(br6);
    document.getElementById("reiniciar").appendChild(leftSApellido);
    document.getElementById("reiniciar").appendChild(campoSApellido);
    document.getElementById("reiniciar").appendChild(br7);
    document.getElementById("reiniciar").appendChild(br8);
    document.getElementById("reiniciar").appendChild(PreguntaSocio);
    document.getElementById("reiniciar").appendChild(inputSocioSi);
    document.getElementById("reiniciar").appendChild(leftSocioSi);
    document.getElementById("reiniciar").appendChild(br9);
    document.getElementById("reiniciar").appendChild(inputSocioNo);
    document.getElementById("reiniciar").appendChild(leftSocioNo);
    document.getElementById("reiniciar").appendChild(br10);
    document.getElementById("reiniciar").appendChild(inputSocioNC);
    document.getElementById("reiniciar").appendChild(leftSocioNC);
    document.getElementById("reiniciar").appendChild(br11);
    document.getElementById("reiniciar").appendChild(br12);
    document.getElementById("reiniciar").appendChild(inputTerminos);
    document.getElementById("reiniciar").appendChild(leftTerminos);
    document.getElementById("reiniciar").appendChild(br13);
    document.getElementById("reiniciar").appendChild(inputPrivacidad);
    document.getElementById("reiniciar").appendChild(leftPrivacidad);
    document.getElementById("reiniciar").appendChild(br14);
    document.getElementById("reiniciar").appendChild(inputNotificaciones);
    document.getElementById("reiniciar").appendChild(leftNotificaciones);
    document.getElementById("reiniciar").appendChild(br15);
    document.getElementById("reiniciar").appendChild(br16);
    document.body.appendChild(botonEnviar);
    document.body.appendChild(botonReiniciar);
 
}

