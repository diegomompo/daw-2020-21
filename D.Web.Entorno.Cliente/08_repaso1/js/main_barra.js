function barraRecogiendo(){
    var header1 = document.createElement("h1");
    header1.innerHTML = "Recogiendo los datos obtenidos por el usuario";
    document.body.appendChild(header1);
    cuerpoBarra()
    move();
}

function barraImprimiendo(){
    var header2 = document.createElement("h1");
    header2.innerHTML = "Imprimiendo los datos obtenidos por el usuario";
    document.body.appendChild(header2);
    cuerpoBarra()   
    move();
}
function barraReiniciando(){
    var header3 = document.createElement("h1");
     header3.innerHTML = "Reiniciando todos los campos";
     document.body.appendChild(header3);
     cuerpoBarra();
     move();
}
function cuerpoBarra(){
    var fondo = document.createElement("div");
    fondo.setAttribute("class", "estiloFondo");
    fondo.id = "eFondo"

    var eBarra = document.createElement("div");
    eBarra.setAttribute("class", "estiloBarra");
    eBarra.id = "barra";

    var ePorcentaje = document.createElement("div");
    ePorcentaje.setAttribute("class", "estiloPorcentaje");
    ePorcentaje.id = "porcentaje";
    ePorcentaje.innerHTML="0%";

    var botonCerrar = document.createElement("button");

    botonCerrar.setAttribute("type", "submit");
    botonCerrar.setAttribute("class", "btn btn-warning");
    botonCerrar.innerHTML = "Cerrar";
    botonCerrar.onclick = cerrar

    document.body.appendChild(fondo);
    document.getElementById("eFondo").appendChild(eBarra);
    document.getElementById("barra").appendChild(ePorcentaje);
   

       document.body.appendChild(botonCerrar);
}