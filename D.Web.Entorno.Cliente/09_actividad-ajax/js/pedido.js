var precioIngedientes = 0; //Variable para sumar los ingredientes extras
var precioTamañoPizza = 0; //Variable para guardar el precio del tamaño de la pizza
var precioTotal = 0; //Importe total del pedido
var imprimePrecio = 0; //Variable que recoge el Importe total de la función


var imprimirTamano = document.createElement("p");

//FUNCION PARA PROCESAR EL PEDIDO E IMPRIMIR EL RESULTADO

function procesando() { //Función que procesa el pedido

    window.open("barra_imprimiendo.html" , "barra_procesando.html" , "width=1920,height=150,top=750,scrollbars=NO"); // Barra de calculando el precio total
    
    CalcularPrecio(); // Llama a la función que calcula el importe total del pedido


    ventana1 = window.open("barra_procesando.html" , "ventana3" , "width=1920,height=150,top=750,scrollbars=NO"); // Barra de cargando que se abre en otra ventana y carga el procesamiento del pedido

    if(precioTotal != 0){ //Comprueba si hay importe o no

        //Imprime el pedido completo

        var textoPrecioTamano = document.createTextNode("El precio del tamaño de la pizza es:   " + precioTamañoPizza + " € ");
            
        imprimirTamano.appendChild(textoPrecioTamano);
        document.body.appendChild(imprimirTamano);

        var imprimirExtra = document.createElement("p");

        var textoImprimirExtra = document.createTextNode("Precio total de los ingrediente extras: " + precioIngedientes + " € ");

        imprimirExtra.appendChild(textoImprimirExtra);
        document.body.appendChild(imprimirExtra);

        alert("El precio total es " + precioTotal + " €");
        alert("Pulse F12 para consultar el pedido entero");

        console.log("Nombre: " + valorNombre.value);
        console.log("Dirección: " + valorDireccion.value);
        console.log("Teléfono: " + valorTelefono.value);
        console.log("Email: " + valorEmail.value);
        console.log("Precio del tamaño de la pizza: " + precioTamañoPizza);
        console.log("Precio de los ingredientes extra : " + precioIngedientes);
        console.log("Precio total del pedido : " + precioTotal);
    }
}

// -------------------------------------------------------------------------

//FUNCION PARA CALCULAR EL PRECIO TOTAL DEL PEDIDO


function CalcularPrecio(){ //Función que calcula el precio total del pedido

        window.open("barra_calculando.html" , "ventana2" , "width=1920,height=150,top=750,scrollbars=NO"); // Barra de calculando el precio total

//-------------------------------------------------------------------------------

// CALCULA EL TAMAÑO DE LA PIZZA

tamano = document.getElementsByName("tamano");

for(var i=0; i<tamano.length;i++){
    if(tamano[i].checked){
        var valorTam = Number(tamano[i].value);
        precioTamañoPizza += valorTam;
    }
}



 
  //-----------------------------------------------------------------------

  //CALCULA EL PRECIO DE LOS INGREDIENTES

    topping = document.getElementsByName("topping");

    for(var i=0; i<topping.length;i++){
        if(topping[i].checked){
            var valorIng = Number(topping[i].value);
            precioIngedientes += valorIng;
        }
    }

//--------------------------------------------------------------------------------

// CALCULA EL PRECIO TOTAL DEL PEDIDO

    precioTotal = precioTamañoPizza + precioIngedientes; //Suma el precio del tamaño de la pizza y el precio de los ingredientes extra

}

// -------------------------------------------------------------------------

//FUNCION PARA LIMPIAR LOS CAMPOS DEL FORMUULARIO


function limpiar(){ //Función para limpiar los campos
    window.open("barra_reiniciando.html" , "ventana1" , "width=1920,height=150,top=750,scrollbars=NO"); // Barra de progreso que indica se está limpiando los campos
    document.getElementById("reiniciar").reset(); //nodo que se llama desde el form para limpiarlo
}

// -------------------------------------------------------------------------

//FUNCION PARA LIMPIAR LOS CAMPOS DEL FORMUULARIO

function actualizar(){
    window.open("barra_actualizando.html" , "ventana1" , "width=1920,height=150,top=750,scrollbars=NO"); // Barra de cargando que se abre en otra ventana y actualiza el formulario
    location.reload();
}