//--------------------------------------------------------------
var disponible = true;

//FUNCION QUE EJECUTA LAS DISTINTAS FUNCIONES QUE TENDRÁ EL HTML

window.onload = function(){

    textoInicioPizzeria();
    obligadoPizzeria();
    formularioPizzeria();
}
//--------------------------------------------------------------------

//FUNCION PARA MOSTRAR EL NOMBRE DE LA TIENDA

function textoInicioPizzeria(){
        header1 = document.createElement("h1")
        textheader1 = document.createTextNode("Pizzería con AJAX");
        header1.appendChild(textheader1);

        header1.setAttribute("class", "nombrePizzeria");

        document.body.appendChild(header1);

}
//----------------------------------------------------------------------

//FUNCION PARA MOSTRAR QUE LOS TEXTOS EN ROJO SON OBLIGATORIOS

function obligadoPizzeria(){

    textObligado = document.createElement("p");
    textObligatorio = document.createTextNode("Los textos en rojo es obligatorio completarlos");
    textObligado.appendChild(textObligatorio);

    document.body.appendChild(textObligado);
}
//-------------------------------------------------------------------------

//FUNCION PARA INICIAR EL FORMULARIO

function formularioPizzeria(){
    formuPizzeria = document.createElement("form");
    formuPizzeria.id = "reiniciar";

    document.body.appendChild(formuPizzeria);

    nombre();
    direccion();
    telefono();
    email();
    mostrarTamanoPizza();
    mostrarIngredientes();
    buttonValidar();
    buttonProcesar();
    buttonLimpiar()
    buttonActualizar();


}



//---------------------------------------------------------------------------

//FUNCIÓN PARA EL CAMPO NOMBRE

function nombre(){  

    var leftNombre = document.createElement("left");
    var textoleftNombre = document.createTextNode("Nombre: ");
    leftNombre.appendChild(textoleftNombre);
    var campoNombre = document.createElement("input");

    leftNombre.setAttribute("class", "obligatorio");
    campoNombre.id = "valorNombre";

    br1 = document.createElement("br");
    br2 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(leftNombre);
    document.getElementById("reiniciar").appendChild(campoNombre);
    document.getElementById("reiniciar").appendChild(br1);
    document.getElementById("reiniciar").appendChild(br2);

}

//---------------------------------------------------------------------------

//FUNCIÓN PARA CAMPO DIRECCIÓN

function direccion(){

    leftDireccion = document.createElement("left");
    textoleftDireccion = document.createTextNode("Dirección: ");
    leftDireccion.appendChild(textoleftDireccion);
    campoDireccion = document.createElement("input");

    leftDireccion.setAttribute("class", "obligatorio");
    campoDireccion.id = "valorDireccion";

    br3 = document.createElement("br");
    br4 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(leftDireccion);
    document.getElementById("reiniciar").appendChild(campoDireccion);
    document.getElementById("reiniciar").appendChild(br3);
    document.getElementById("reiniciar").appendChild(br4);

}

//------------------------------------------------------------------------------

//FUNCION PARA CAMPO TELEFONO

function telefono(){

    leftTelefono = document.createElement("left");
    textoleftTelefono = document.createTextNode("Teléfono: ");
    leftTelefono.appendChild(textoleftTelefono);
    campoTelefono = document.createElement("input");

    leftTelefono.setAttribute("class", "obligatorio");
    campoTelefono.setAttribute("type", "number");
    campoTelefono.id = "valorTelefono";

    br5 = document.createElement("br");
    br6 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(leftTelefono);
    document.getElementById("reiniciar").appendChild(campoTelefono);
    document.getElementById("reiniciar").appendChild(br5);
    document.getElementById("reiniciar").appendChild(br6);

}

//---------------------------------------------------------------------------------

//FUNCION PARA CAMPO EMAIL

function email(){

    leftEmail = document.createElement("left");
    textoleftEmail = document.createTextNode("Email: ");
    leftEmail.appendChild(textoleftEmail);
    campoEmail = document.createElement("input");

    leftEmail.setAttribute("class", "obligatorio");
    campoEmail.id = "valorEmail";

    br7 = document.createElement("br");
    br8 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(leftEmail);
    document.getElementById("reiniciar").appendChild(campoEmail);
    document.getElementById("reiniciar").appendChild(br7);
    document.getElementById("reiniciar").appendChild(br8);
}
//---------------------------------------------------------------------------------

//FUNCION TAMAÑO DE LA PIZZA
function  mostrarTamanoPizza(){

    tamanodiv = document.createElement("div");
    tamanodiv.id = "tamPizza";

    document.getElementById("reiniciar").appendChild(tamanodiv);

    tamanoPizza();
}

function tamanoPizza(){

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        respuestaTamano(this.responseText);
    }
  };
  xhttp.open("GET", "http://127.0.0.1:5501/json/tamano_pizza.json", true);
  xhttp.send();
  
}

function respuestaTamano(jsonDoc) {
    var objetoJsonTamano = JSON.parse(jsonDoc);
    console.log(objetoJsonTamano)
    var arrayTamano = objetoJsonTamano.pizza.tamano;
    var arrayMostrar = objetoJsonTamano.pizza.mostrarTamano;
    var arrayEspacio = objetoJsonTamano.pizza.espacio;

    for(let mostrarTamano of arrayMostrar){

    pTamano = document.createElement(mostrarTamano.elementP);
    textpTamano = document.createTextNode(mostrarTamano.titleTamano);
    pTamano.appendChild(textpTamano);
    pTamano.setAttribute("class", mostrarTamano.classObligado);

    document.getElementById("tamPizza").appendChild(pTamano);

    }

    for(let tamano of arrayTamano){
            radioTamano = document.createElement(tamano.elementInput);
            radioTamano.setAttribute("type", tamano.type);
            radioTamano.setAttribute("name", tamano.name);

            leftPizza = document.createElement(tamano.elementLeft);
            textoleftPizza = document.createTextNode(tamano.title);
            leftPizza.appendChild(textoleftPizza);

            br9 = document.createElement(tamano.espacio);

            document.getElementById("tamPizza").appendChild(radioTamano);
            document.getElementById("tamPizza").appendChild(leftPizza);
            document.getElementById("tamPizza").appendChild(br9);
    }
}


//-------------------------------------------------------------------------------------

// FUNCION TEXTO INGREDIENTES

function mostrarIngredientes(){

    var extradiv = document.createElement("div");
    extradiv.id = "extras";

    document.getElementById("reiniciar").appendChild(extradiv);

   ingExtra();
}

function ingExtra(){

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        respuestaExtra(this.responseText);
    }
  };
  xhttp.open("GET", "http://127.0.0.1:5501/json/ingredientes.json", true);
  xhttp.send();
}

function respuestaExtra(jsonDoc){
    var objetoJsonExtra = JSON.parse(jsonDoc);
    console.log(objetoJsonExtra);

    var arrayPExtra = objetoJsonExtra.extra.elementpExtra;
    var arrayExtra = objetoJsonExtra.extra.ingredientesExt;
    var arraybrExtra = objetoJsonExtra.extra.espacioExtra;

    for(let elementpExtra of arrayPExtra){

    pExtra = document.createElement(elementpExtra.elementP);
    textpExtra = document.createTextNode(elementpExtra.titleExtra);
    pExtra.appendChild(textpExtra);
    pExtra.setAttribute("class", elementpExtra.classObligado);

    document.getElementById("extras").appendChild(pExtra);

    }

    for(let ingredientesExt of arrayExtra){
        divExtra = document.createElement(ingredientesExt.elementDiv);
        divExtra.id = ingredientesExt.idDiv;

        leftExtraNombre = document.createElement(ingredientesExt.elementLeft);
        textoExtraNombre = document.createTextNode(ingredientesExt.titleNombre);
        leftExtraNombre.appendChild(textoExtraNombre);

        inputExtra = document.createElement(ingredientesExt.elementInput);
        inputExtra.setAttribute("type", ingredientesExt.type);
        inputExtra.setAttribute("name", ingredientesExt.name);;
        
        leftExtraPrecio = document.createElement(ingredientesExt.elementLeft);
        textoExtraPrecio = document.createTextNode(ingredientesExt.titlePrecio);
        leftExtraPrecio.appendChild(textoExtraPrecio)

        document.getElementById("extras").appendChild(divExtra);
        document.getElementById("extras").appendChild(leftExtraNombre);
        document.getElementById("extras").appendChild(inputExtra);
        document.getElementById("extras").appendChild(leftExtraPrecio);

    }

    for(let espacioExtra of arraybrExtra){
        espExtra = document.createElement(espacioExtra.espacio);
        espExtra1 = document.createElement(espacioExtra.espacio);
        document.getElementById("extras").appendChild(espExtra);
        document.getElementById("extras").appendChild(espExtra1);
    }

}

//----------------------------------------------------------------------------------

//FUNCIÓN BOTÓN VALIDAR

function  buttonValidar(){

    botonValidar = document.createElement("button");
    textbotonValidar = document.createTextNode("Validar");
    botonValidar.appendChild(textbotonValidar);


    (botonValidar.addEventListener("click", validar));
    
    

    document.body.appendChild(botonValidar);
}

//----------------------------------------------------------------------------------

//FUNCIÓN BOTÓN ENVIAR

function  buttonProcesar(){

    botonProcesar = document.createElement("button");
    textbotonProcesar = document.createTextNode("Procesar");
    botonProcesar.appendChild(textbotonProcesar);

    botonProcesar.disabled=true;
    botonProcesar.id = "validacion";


    botonProcesar.addEventListener("click", procesando);


    document.body.appendChild(botonProcesar);
}

//-------------------------------------------------------------------------------------

//FUNCIÓN BOTÓN LIMPIAR

function  buttonLimpiar(){

    botonLimpiar = document.createElement("button");
    textbotonLimpiar = document.createTextNode("Limpiar");
    botonLimpiar.appendChild(textbotonLimpiar);

    botonLimpiar.addEventListener("click", limpiar);

    document.body.appendChild(botonLimpiar);
}

//---------------------------------------------------------------------------------------

//FUNCIÓN BOTÓN ACTUALIZAR

function buttonActualizar(){

    botonActualizar = document.createElement("button");
    textbotonActualizar = document.createTextNode("Actualizar");
    botonActualizar.appendChild(textbotonActualizar);

    botonActualizar.addEventListener("click", actualizar);

    document.body.appendChild(botonActualizar);
}



//---------------------------------------------------------------------------------------



















































































