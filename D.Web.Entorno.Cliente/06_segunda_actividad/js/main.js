function cuerpo(){
    textoInicio();
    obligado();
    formulario();
}

function textoInicio(){
        header1 = document.createElement("h1")
        textheader1 = document.createTextNode("Actividad: DOM y Formularios");
        header1.appendChild(textheader1);
        document.body.appendChild(header1);

}
function obligado(){

    textObligado = document.createElement("p");
    textObligatorio = document.createTextNode("Los textos en rojo es obligatorio completarlos");
    textObligado.appendChild(textObligatorio);
    document.body.appendChild(textObligado);
}
function formulario(){

    //INICIO FORMULARIO

    var formu = document.createElement("form");
    formu.id = "reiniciar";

    document.body.appendChild(formu);

    //------------------------------------------------------------------------------------------------------------------

    // CAMPO DNI

    var leftDNI = document.createElement("left")
    var textoleftDNI = document.createTextNode("DNI: ");
    leftDNI.appendChild(textoleftDNI);
     var campoDNI = document.createElement("input");

    leftDNI.setAttribute("class", "obligatorio");
    campoDNI.setAttribute("class", "cuadro");
    campoDNI.id = "textoDNI";

     br1 = document.createElement("br");
     br2 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(leftDNI);
    document.getElementById("reiniciar").appendChild(campoDNI);
    document.getElementById("reiniciar").appendChild(br1);
    document.getElementById("reiniciar").appendChild(br2);

    //------------------------------------------------------------------------------------------------------------------

    // CAMPO NOMBRE
    

     leftNombre = document.createElement("left");
     textoleftNombre= document.createTextNode("Nombre: ");
    leftNombre.appendChild(textoleftNombre);
    campoNombre = document.createElement("input");

    leftNombre.setAttribute("class", "obligatorio");
    campoNombre.setAttribute("class", "cuadro");
    campoNombre.id = "textoNombre";
    
    br3 = document.createElement("br");
    br4 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(leftNombre);
    document.getElementById("reiniciar").appendChild(campoNombre);
    document.getElementById("reiniciar").appendChild(br3);
    document.getElementById("reiniciar").appendChild(br4);
    
    //------------------------------------------------------------------------------------------------------------------

    // CAMPO PRIMER APELLIDO
    

     leftPApellido = document.createElement("left");
    textoleftPApellido= document.createTextNode( "Primer Apellido: ");
    leftPApellido.appendChild(textoleftPApellido);
     campoPApellido = document.createElement("input");

    leftPApellido.setAttribute("class", "obligatorio");
    campoPApellido.setAttribute("class", "cuadro");
    campoPApellido.id = "textoApellido1";

     br5 = document.createElement("br");
     br6 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(leftPApellido);
    document.getElementById("reiniciar").appendChild(campoPApellido);
    document.getElementById("reiniciar").appendChild(br5);
    document.getElementById("reiniciar").appendChild(br6);

     //------------------------------------------------------------------------------------------------------------------

    // CAMPO SEGUNDO APELLIDO
    

     leftSApellido= document.createElement("left");
    textoleftSApellido= document.createTextNode( "Segundo Apellido: ");
    leftSApellido.appendChild(textoleftSApellido);
     campoSApellido = document.createElement("input");

    campoSApellido.setAttribute("class", "cuadro");
    campoSApellido.id = "textoApellido2";


     br7 = document.createElement("br");
     br8 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(leftSApellido);
    document.getElementById("reiniciar").appendChild(campoSApellido);
    document.getElementById("reiniciar").appendChild(br7);
    document.getElementById("reiniciar").appendChild(br8);

     //------------------------------------------------------------------------------------------------------------------

    //  PEGUNTA SOCIO
    

     PreguntaSocio = document.createElement("p");
    textoPreguntaSocio = document.createTextNode( "¿Desea hacerse socio?");
    PreguntaSocio.appendChild(textoPreguntaSocio);

    PreguntaSocio.setAttribute("class", "obligatorio");

    document.getElementById("reiniciar").appendChild(PreguntaSocio);

    //------------------------------------------------------------------------------------------------------------------

    //  SOCIO SI
    

     leftSocioSi= document.createElement("left");
    textoleftSocioSi= document.createTextNode( "Si");
    leftSocioSi.appendChild(textoleftSocioSi);

   inputSocioSi = document.createElement("input");
    inputSocioSi.setAttribute("type", "radio");
    inputSocioSi.setAttribute("value", "si");
    inputSocioSi.setAttribute("name", "pregunta");
    inputSocioSi.id = "pregunta_si";

    br9 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(inputSocioSi);
    document.getElementById("reiniciar").appendChild(leftSocioSi);
    document.getElementById("reiniciar").appendChild(br9);

    //------------------------------------------------------------------------------------------------------------------

    // SOCIO NO
    

  leftSocioNo= document.createElement("left");
    textoleftSocioNo= document.createTextNode("No");
    leftSocioNo.appendChild(textoleftSocioNo);

    inputSocioNo = document.createElement("input");;
    inputSocioNo.setAttribute("type", "radio");
    inputSocioNo.setAttribute("value", "no");
    inputSocioNo.setAttribute("name", "pregunta");;
    inputSocioNo.id = "pregunta_no";

   br10 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(inputSocioNo);
    document.getElementById("reiniciar").appendChild(leftSocioNo);
    document.getElementById("reiniciar").appendChild(br10);

   //------------------------------------------------------------------------------------------------------------------

    // SOCIO NC
    

     leftSocioNC= document.createElement("left");
    textoleftSocioNC= document.createTextNode( "NC ");
    leftSocioNC.appendChild(textoleftSocioNC);

    inputSocioNC = document.createElement("input");
    inputSocioNC.setAttribute("type", "radio");
    inputSocioNC.setAttribute("value", "NC");
    inputSocioNC.setAttribute("name", "pregunta");;
    inputSocioNC.id = "pregunta_nc";

     br11 = document.createElement("br");
     br12 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(inputSocioNC);
    document.getElementById("reiniciar").appendChild(leftSocioNC);
    document.getElementById("reiniciar").appendChild(br11);
    document.getElementById("reiniciar").appendChild(br12);

     //------------------------------------------------------------------------------------------------------------------

    // ACEPTAR TERMINOS Y CONDICIONES
    


    leftTerminos= document.createElement("left");
    textoleftTerminos= document.createTextNode( "He leído y aceptado los términos y condiciones");
    leftTerminos.appendChild(textoleftTerminos);

     inputTerminos = document.createElement("input");
    inputTerminos.setAttribute("type", "checkbox");

    leftTerminos.setAttribute("class", "obligatorio");

    inputTerminos.id = "condiciones";

     br13 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(inputTerminos);
    document.getElementById("reiniciar").appendChild(leftTerminos);
    document.getElementById("reiniciar").appendChild(br13);

     //------------------------------------------------------------------------------------------------------------------

    // ACEPTAR POLITICA DE PRIVACIDAD
    

     leftPrivacidad= document.createElement("left");

    textoleftPrivacidad= document.createTextNode( "He leído y aceptado la política de privacidad ");
    leftPrivacidad.appendChild(textoleftPrivacidad);

    inputPrivacidad = document.createElement("input");

    inputPrivacidad.setAttribute("type", "checkbox");

    leftPrivacidad.setAttribute("class", "obligatorio");

    inputPrivacidad.id = "privacidad";

    br14 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(inputPrivacidad);
    document.getElementById("reiniciar").appendChild(leftPrivacidad);
    document.getElementById("reiniciar").appendChild(br14)

    //------------------------------------------------------------------------------------------------------------------

    // ACEPTAR O NO NOTIFICACIONES
    

    leftNotificaciones= document.createElement("left");

    textoleftNotificaciones= document.createTextNode( "Deseo recibir notificaciones sobre el sitio web");
    leftNotificaciones.appendChild(textoleftNotificaciones);

    inputNotificaciones = document.createElement("input");

    inputNotificaciones.setAttribute("type", "checkbox");

    leftNotificaciones.setAttribute("class", "obligatorio");

    inputNotificaciones.id = "notificaciones";

    br15 = document.createElement("br");
    br16 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(inputNotificaciones);
    document.getElementById("reiniciar").appendChild(leftNotificaciones);
    document.getElementById("reiniciar").appendChild(br15);
    document.getElementById("reiniciar").appendChild(br16);

    //------------------------------------------------------------------------------------------------------------------

    // BOTON ENVIAR 

    botonEnviar = document.createElement("button");

    botonEnviar.onclick = recogiendo

    textobotonEnviar= document.createTextNode( "Enviar");


    botonEnviar.appendChild(textobotonEnviar);


    document.body.appendChild(botonEnviar);

    //------------------------------------------------------------------------------------------------------------------

    // BOTON REINICIAR
    

    botonReiniciar = document.createElement("button");

    botonReiniciar.onclick = reinicio

    textobotonReiniciar= document.createTextNode( "Limpiar Campos");

    botonReiniciar.appendChild(textobotonReiniciar);

    document.body.appendChild(botonReiniciar);

}

