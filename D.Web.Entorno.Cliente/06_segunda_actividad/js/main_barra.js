// FUNCION PARA MOSTRAR EL TEXTO DE LA BARRA RECOGIENDO

function barraRecogiendo(){
    var header1 = document.createElement("h1");
    textheader1 = document.createTextNode("Recogiendo los datos obtenidos por el usuario");
    header1.appendChild(textheader1);
    document.body.appendChild(header1);
    cuerpoBarra()
    move();
}
//-------------------------------------------------------------------------------------------------

// FUNCION PARA MOSTRAR EL TEXTO DE LA BARRA IMPRIMIR

function barraImprimiendo(){
    var header2 = document.createElement("h1");
    textheader2 = document.createTextNode("Recogiendo los datos obtenidos por el usuario");
    header2.appendChild(textheader2);
    document.body.appendChild(header2);
    cuerpoBarra()   
    move();
}

//-------------------------------------------------------------------------------------------------

// FUNCION PARA MOSTRAR EL TEXTO DE LA BARRA REINICIAR

function barraReiniciando(){
    var header3 = document.createElement("h1");
    textheader3 = document.createTextNode("Recogiendo los datos obtenidos por el usuario");
    header3.appendChild(textheader3);
     document.body.appendChild(header3);
     cuerpoBarra();
     move();
}

//-------------------------------------------------------------------------------------------------

// FUNCION QUE DEFINE EL CUERPO DE LA BARRA Y EK BOTON DE CERRAR LA BARRA

function cuerpoBarra(){
    var fondo = document.createElement("div");
    fondo.setAttribute("class", "estiloFondo");
    fondo.id = "eFondo"

    var eBarra = document.createElement("div");
    eBarra.setAttribute("class", "estiloBarra");
    eBarra.id = "barra";

    var ePorcentaje = document.createElement("div");
    ePorcentaje.setAttribute("class", "estiloPorcentaje");
    ePorcentaje.id = "porcentaje";
    ePorcentaje.innerHTML="0%";

    var botonCerrar = document.createElement("button");

    botonCerrar.setAttribute("type", "submit");
    botonCerrar.setAttribute("class", "btn btn-warning");
    botonCerrar.innerHTML = "Cerrar";
    botonCerrar.onclick = cerrar

    document.body.appendChild(fondo);
    document.getElementById("eFondo").appendChild(eBarra);
    document.getElementById("barra").appendChild(ePorcentaje);
   

       document.body.appendChild(botonCerrar);
}