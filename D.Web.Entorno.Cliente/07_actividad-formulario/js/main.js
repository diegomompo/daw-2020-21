//--------------------------------------------------------------
var disponible = true;

//FUNCION QUE EJECUTA LAS DISTINTAS FUNCIONES QUE TENDRÁ EL HTML

window.onload = function(){

    textoInicioPizzeria();
    obligadoPizzeria();
    formularioPizzeria();
}
//--------------------------------------------------------------------

//FUNCION PARA MOSTRAR EL NOMBRE DE LA TIENDA

function textoInicioPizzeria(){
        header1 = document.createElement("h1")
        textheader1 = document.createTextNode("Pizzería Mompó");
        header1.appendChild(textheader1);

        header1.setAttribute("class", "nombrePizzeria");

        document.body.appendChild(header1);

}
//----------------------------------------------------------------------

//FUNCION PARA MOSTRAR QUE LOS TEXTOS EN ROJO SON OBLIGATORIOS

function obligadoPizzeria(){

    textObligado = document.createElement("p");
    textObligatorio = document.createTextNode("Los textos en rojo es obligatorio completarlos");
    textObligado.appendChild(textObligatorio);

    document.body.appendChild(textObligado);
}
//-------------------------------------------------------------------------

//FUNCION PARA INICIAR EL FORMULARIO

function formularioPizzeria(){
    formuPizzeria = document.createElement("form");
    formuPizzeria.id = "reiniciar";

    document.body.appendChild(formuPizzeria);

    nombre();
    direccion();
    telefono();
    email();
    preguntaTamano();
    elegirTamano();
    textoIngredientes();
    elegirIngredientes();
    buttonValidar();
    buttonProcesar();
    buttonLimpiar();


}



//---------------------------------------------------------------------------

//FUNCIÓN PARA EL CAMPO NOMBRE

function nombre(){  

    var leftNombre = document.createElement("left");
    var textoleftNombre = document.createTextNode("Nombre: ");
    leftNombre.appendChild(textoleftNombre);
    var campoNombre = document.createElement("input");

    leftNombre.setAttribute("class", "obligatorio");
    campoNombre.id = "valorNombre";

    br1 = document.createElement("br");
    br2 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(leftNombre);
    document.getElementById("reiniciar").appendChild(campoNombre);
    document.getElementById("reiniciar").appendChild(br1);
    document.getElementById("reiniciar").appendChild(br2);

}

//---------------------------------------------------------------------------

//FUNCIÓN PARA CAMPO DIRECCIÓN

function direccion(){

    leftDireccion = document.createElement("left");
    textoleftDireccion = document.createTextNode("Dirección: ");
    leftDireccion.appendChild(textoleftDireccion);
    campoDireccion = document.createElement("input");

    leftDireccion.setAttribute("class", "obligatorio");
    campoDireccion.id = "valorDireccion";

    br3 = document.createElement("br");
    br4 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(leftDireccion);
    document.getElementById("reiniciar").appendChild(campoDireccion);
    document.getElementById("reiniciar").appendChild(br3);
    document.getElementById("reiniciar").appendChild(br4);

}

//------------------------------------------------------------------------------

//FUNCION PARA CAMPO TELEFONO

function telefono(){

    leftTelefono = document.createElement("left");
    textoleftTelefono = document.createTextNode("Teléfono: ");
    leftTelefono.appendChild(textoleftTelefono);
    campoTelefono = document.createElement("input");

    leftTelefono.setAttribute("class", "obligatorio");
    campoTelefono.setAttribute("type", "number");
    campoTelefono.id = "valorTelefono";

    br5 = document.createElement("br");
    br6 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(leftTelefono);
    document.getElementById("reiniciar").appendChild(campoTelefono);
    document.getElementById("reiniciar").appendChild(br5);
    document.getElementById("reiniciar").appendChild(br6);

}

//---------------------------------------------------------------------------------

//FUNCION PARA CAMPO EMAIL

function email(){

    leftEmail = document.createElement("left");
    textoleftEmai = document.createTextNode("Email: ");
    leftEmail.appendChild(textoleftEmai);
    campoEmail = document.createElement("input");

    leftEmail.setAttribute("class", "obligatorio");
    campoEmail.id = "valorEmail";

    br7 = document.createElement("br");
    br8 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(leftEmail);
    document.getElementById("reiniciar").appendChild(campoEmail);
    document.getElementById("reiniciar").appendChild(br7);
    document.getElementById("reiniciar").appendChild(br8);
}

//--------------------------------------------------------------------------------

// FUNCIÓN PREGUNTA TAMAÑO PIZZA

function preguntaTamano(){

    pTamano = document.createElement("p");
    textopTamano = document.createTextNode("Elige el tamaño de la pizza");
    pTamano.appendChild(textopTamano);

    pTamano.setAttribute("class", "obligatorio");
    pTamano.id = "tamanoPizza";

    document.getElementById("reiniciar").appendChild(pTamano);

}


//--------------------------------------------------------------------------------

// FUNCIÓN ELEGIR TAMAÑO PIZZA

function elegirTamano(){

    // PIZZA PEQUEÑA

    radioPequena = document.createElement("input");
    leftPequena = document.createElement("left");
    textoleftPequena = document.createTextNode("Pizza Pequeña (5€)");
    leftPequena.appendChild(textoleftPequena);

    radioPequena.setAttribute("type", "radio");
    radioPequena.setAttribute("name", "tamano");
    radioPequena.id = "pequeña";

    br9 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(radioPequena);
    document.getElementById("reiniciar").appendChild(leftPequena);
    document.getElementById("reiniciar").appendChild(br9);

//--------------------------------------------------------------------------------

    // PIZZA MEDIANA

    radioMediana = document.createElement("input");
    leftMediana= document.createElement("left");
    textoleftMediana = document.createTextNode("Pizza Mediana (10€)");
    leftMediana.appendChild(textoleftMediana);

    radioMediana.setAttribute("type", "radio");
    radioMediana.setAttribute("name", "tamano");
    radioMediana.id = "mediana";

    br10 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(radioMediana);
    document.getElementById("reiniciar").appendChild(leftMediana);
    document.getElementById("reiniciar").appendChild(br10); 

//--------------------------------------------------------------------------------

    // PIZZA GRANDE

    radioGrande = document.createElement("input");
    leftGrande= document.createElement("left");
    textoleftGrande = document.createTextNode("Pizza Grande (15€)");
    leftGrande.appendChild(textoleftGrande);

    radioGrande.setAttribute("type", "radio");
    radioGrande.setAttribute("name", "tamano");
    radioGrande.id = "grande";

    br11 = document.createElement("br");
    br12 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(radioGrande);
    document.getElementById("reiniciar").appendChild(leftGrande);
    document.getElementById("reiniciar").appendChild(br11);
    document.getElementById("reiniciar").appendChild(br12);

}
//---------------------------------------------------------------------------------

// FUNCION TEXTO INGREDIENTES

function textoIngredientes(){

    pIngredientes = document.createElement("p");
    textopIngredientes = document.createTextNode("Ingredientes (1€ cada uno)");
    pIngredientes.appendChild(textopIngredientes);

    pIngredientes.setAttribute("class", "obligatorio");
    pIngredientes.setAttribute("name", "tamano");
    pIngredientes.id = "tamanoPizza";

    document.getElementById("reiniciar").appendChild(pIngredientes);

}

//---------------------------------------------------------------------------------

//FUNCION ELEGIR INGREDIENTES

function elegirIngredientes(){

    //INGREDIENTE TOMATE

    checkboxTomate = document.createElement("input");
    leftTomate = document.createElement("left");
    textoleftTomate = document.createTextNode("Tomate");
    leftTomate.appendChild(textoleftTomate);

    checkboxTomate.setAttribute("type", "checkbox");
    checkboxTomate.setAttribute("name", "extra");
    checkboxTomate.id = "tomate";

    br13 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(checkboxTomate);
    document.getElementById("reiniciar").appendChild(leftTomate);
    document.getElementById("reiniciar").appendChild(br13);

//----------------------------------------------------------------------------------

    //INGREDIENTE QUESO

    checkboxQueso = document.createElement("input");
    leftQueso = document.createElement("left");
    textoleftQueso = document.createTextNode("Queso");
    leftQueso.appendChild(textoleftQueso);

    checkboxQueso.setAttribute("type", "checkbox");
    checkboxQueso.setAttribute("name", "extra");
    checkboxQueso.id = "queso";

    br14 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(checkboxQueso);
    document.getElementById("reiniciar").appendChild(leftQueso);
    document.getElementById("reiniciar").appendChild(br14);

//----------------------------------------------------------------------------------

    //INGREDIENTE PIÑA

    checkboxPina = document.createElement("input");
    leftPina = document.createElement("left");
    textoleftPina = document.createTextNode("Piña");
    leftPina.appendChild(textoleftPina);

    checkboxPina.setAttribute("type", "checkbox");
    checkboxPina.setAttribute("name", "extra");
    checkboxPina.id = "pina";

    br15 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(checkboxPina);
    document.getElementById("reiniciar").appendChild(leftPina);
    document.getElementById("reiniciar").appendChild(br15);

    //----------------------------------------------------------------------------------

    //INGREDIENTE HUEVO

    checkboxHuevo = document.createElement("input");
    leftHuevo = document.createElement("left");
    textoleftHuevo = document.createTextNode("Huevo");
    leftHuevo.appendChild(textoleftHuevo);

    checkboxHuevo.setAttribute("type", "checkbox");
    checkboxHuevo.setAttribute("name", "extra");
    checkboxHuevo.id = "huevo";

    br16 = document.createElement("br");
    br17 = document.createElement("br");

    document.getElementById("reiniciar").appendChild(checkboxHuevo);
    document.getElementById("reiniciar").appendChild(leftHuevo);
    document.getElementById("reiniciar").appendChild(br16);
    document.getElementById("reiniciar").appendChild(br17);
}

//----------------------------------------------------------------------------------

//FUNCIÓN BOTÓN VALIDAR

function  buttonValidar(){

    botonValidar = document.createElement("button");
    textbotonValidar = document.createTextNode("Validar");
    botonValidar.appendChild(textbotonValidar);


    (botonValidar.addEventListener("click", validar));
    

    document.body.appendChild(botonValidar);
}

//----------------------------------------------------------------------------------

//FUNCIÓN BOTÓN ENVIAR

function  buttonProcesar(){

    botonProcesar = document.createElement("button");
    textbotonProcesar = document.createTextNode("Procesar");
    botonProcesar.appendChild(textbotonProcesar);

    botonProcesar.disabled=true;
    botonProcesar.id = "validacion";



    botonProcesar.addEventListener("click", procesando);


    document.body.appendChild(botonProcesar);
}
//-------------------------------------------------------------------------------------

//FUNCIÓN BOTÓN LIMPIAR

function  buttonLimpiar(){

    botonLimpiar = document.createElement("button");
    textbotonLimpiar = document.createTextNode("Limpiar");
    botonLimpiar.appendChild(textbotonLimpiar);

    botonLimpiar.addEventListener("click", limpiar);

    document.body.appendChild(botonLimpiar);
}























































































