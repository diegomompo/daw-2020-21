var precioIngedientes = 0; //Variable para sumar los ingredientes extras
var precioTamañoPizza = 0; //Variable para guardar el precio del tamaño de la pizza
var precioTotal = 0; //Importe total del pedido
var imprimePrecio = 0; //Variable que recoge el Importe total de la función

//FUNCION PARA PROCESAR EL PEDIDO E IMPRIMIR EL RESULTADO

function procesando() { //Función que procesa el pedido

    window.open("barra_procesando.html" , "ventana3" , "width=1920,height=150,scrollbars=NO"); // Barra de cargando que se abre en otra ventana y carga el procesamiento del pedido
    
    imprimePrecio = CalcularPrecio(); // Llama a la función que calcula el importe total del pedido

    window.open("barra_imprimiendo.html" , "ventana1" , "width=1920,height=150,scrollbars=NO"); // Barra de calculando el precio total

    if(imprimePrecio != 0){ //Comprueba si hay importe o no
     
        alert("El precio del pedido es: " + imprimePrecio + " €. Pulsa la tecla F12 para ver el pedido completo"); // Imprime el importe total del pedido

        //Imprime el pedido completo

        console.log("Nombre: " + valorNombre.value);
        console.log("Dirección: " + valorDireccion.value);
        console.log("Teléfono: " + valorTelefono.value);
        console.log("Email: " + valorEmail.value);
        console.log("Pizza pequeña: " + pequeña.checked);
        console.log("Pizza mediana: " + mediana.checked);
        console.log("Pizza grande: " + grande.checked);
        console.log("Tomate como Ingrediente: " + tomate.checked);
        console.log("Queso como Ingrediente: " + queso.checked);
        console.log("Pina como Ingrediente: " + pina.checked);
        console.log("Huevo como Ingrediente: " + huevo.checked);
    
    }
}

// -------------------------------------------------------------------------

//FUNCION PARA CALCULAR EL PRECIO TOTAL DEL PEDIDO


function CalcularPrecio(){ //Función que calcula el precio total del pedido

    window.open("barra_calculando.html" , "ventana2" , "width=1920,height=150,scrollbars=NO"); // Barra de calculando el precio total

    if(pequeña.checked == true){
        precioTamañoPizza = 5;
    }
    else if(mediana.checked == false){
        precioTamañoPizza = 10;
    }
    else if(grande.checked == true){
        precioTamañoPizza = 15;
    }
 
    extra = document.getElementsByName("extra");

    for(var i=0; i<extra.length;i++){
        if(extra[i].checked){
            precioIngedientes += 1;
        }
    }

        precioTotal = precioTamañoPizza + precioIngedientes; //Suma el precio del tamaño de la pizza y el precio de los ingredientes extra
        return precioTotal; //Devuelve a la función de arriba el precio total
    
}

// -------------------------------------------------------------------------

//FUNCION PARA LIMPIAR LOS CAMPOS DEL FORMUULARIO


function limpiar(){ //Función para limpiar los campos
    window.open("barra_reiniciando.html" , "ventana1" , "width=1920,height=150,scrollbars=NO"); // Barra de progreso que indica se está limpiando los campos
    validacion.disabled = true;
    document.getElementById("reiniciar").reset(); //nodo que se llama desde el form para limpiarlo
}