/* NAN */
var numero1 = 0;   var numero2 = 0;
alert(numero1/numero2);   //muestra el valor NaN

/* ISNAN */
var num1 = 0;
var num2 = 0;
if(isNaN(num1/num2)) {alert("La división no está definida para estos números");}
else {alert("La división es igual a => " + numero1/numero2);}

/* TOFIXED*/
var numero1 = 4564.34567;
numero1.toFixed(2);   // 4564.35
numero1.toFixed(6);   // 4564.345670
numero1.toFixed();   // 4564