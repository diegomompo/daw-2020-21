/* LENGHT */
var mensaje = "Hola Mundo";
var numLetras = mensaje.length; // numLetras = 10

/* + */
var variable1 = "Hola ";  var variable2 = 3;
var mensaje = variable1 + variable2; // mensaje = "Hola 3"

var mensaje1 = "Hola";
var mensaje2 = "usuario";
var mensaje = mensaje1 + " " + mensaje2; // mensaje = "Hola usuario"

/* UPPER & LOWER */
var mensaje1 = "HolA";
var mensaje2 = mensaje1.toUpperCase(); // mensaje2 = "HOLA"
var mensaje2 = mensaje1.toLowerCase(); // mensaje2 = "hola"

/* CHARAT */
var mensaje = "Hola";
var letra = mensaje.charAt(0); // letra = H
letra = mensaje.charAt(2);     // letra = l

/* INDEXOF & LASTINDEXOF */
var mensaje = "Hola";
var posicion = mensaje.indexOf('a'); // posicion = 3
posicion = mensaje.indexOf('b');     // posicion = -1

var mensaje = "Hola";
var posicion = mensaje.lastIndexOf('a'); // posicion = 3
posicion = mensaje.lastIndexOf('b');     // posicion = -1

/* SUBSTRING */
var mensaje = "Hola usuario";
var porcion = mensaje.substring(2); // porcion = "la usuario"
porcion = mensaje.substring(5);     // porcion = "usuario"
porcion = mensaje.substring(7);     // porcion = "rio"

var porcion = mensaje.substring(5, 0); // porcion = "Hola "
porcion = mensaje.substring(0, 5);     // porcion = "Hola "

/* SPLIT */
var mensaje = "¡¡Hola usuario, soy una cadena de texto!!";
var palabras = mensaje.split(" ");
// palabras = ["¡¡Hola", "usuario,", "soy", "una", "cadena", "de", "texto!!"];

var palabra = "Hola";
var letras = palabra.split(""); // letras = ["H", "o", "l", "a"]