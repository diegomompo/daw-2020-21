//Hola
/* Adiós */


//Enteros y float
var num_1 = 2;
var num_2 = 2;
var total = 150.59;


//Cadenas de texto
var mensaje = "Bienvenido al sitio web de zumos";
var NombreProducto = 'Producto';
var letraSeccionada = 'x';
var texto1 = 'Una frase con \'comillas simples\' dentro';
var texto2 = "Una frase con \"comillas dobles\" dentro";


//Array
var numeros = [1,2,3,4,5,6,7];
var dias = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];
var diaSeleccionado = [0];
var otroDia = [5];

//Boolean
var clienteRegistrado = false;
var ivaIncluido = true;
