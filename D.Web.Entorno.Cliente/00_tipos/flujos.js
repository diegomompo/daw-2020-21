
// IF
var mensaje = true;

if(mensaje) {
    print("Hola usuario")
}

if(mostrarmensaje = false){

}

var mostrando = false; 
var permiteMensajes = true;

if(!mostrando && permiteMensajes){
    print("Es la primera vez que se le muestra al usuario.")
}

//IF - ELSE
var nombre = "";
 
if(nombre == ""){ 
    print("¿Cuál es tu nombre de pila");
}
else {
    print("Hemos guardado tu nombre respetando la LPD");
}

if(edad < 12) {
    print("Todavía eres muy pequeño para entrar en este programa");
}
else if(edad < 19) {
    print("Estás en plena adolescencia");
}
else if(edad < 35) {
    print("Aun sigues siendo joven, ¡¡sigue así!!");
}
else {
    print("Piensa en cuidarte un poco más");
}

//WHILE

var resultado = 0;  var num = 10;  var i = 0;
 
while(i <= num) {
  resultado += i;
  i++;
}

// DO-WHILE

var resultado = 1;   var num = 5;
 
do {
  resultado *= num;    // resultado = resultado * num
  num--;
} while(num > 0);

//SWITCH

switch(variable) {
    case valor_1:
      ...
      break;
    case valor_2:
      ...
      break;
    ...
    case valor_n:
      ...
      break;
    default:
      ...
      break;
  } 

  