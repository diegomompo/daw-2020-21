/* LENGHT */
var vocales = ["a", "e", "i", "o", "u"];
var numVocales = vocales.length; // numVocales = 5

/* CONCAT */
var array1 = [1, 2, 3];
array2 = array1.concat(4, 5, 6);     // array2 = [1, 2, 3, 4, 5, 6]
array3 = array1.concat([4, 5, 6]);   // array3 = [1, 2, 3, 4, 5, 6]

/* JOIN */
var array = ["hola", "ususario"];
var mensaje = array.join(""); // mensaje = "holausuario"
mensaje = array.join(" ");    // mensaje = "hola usuario"

/* POP & PUSH */
var array = [1, 2, 3];
var ultimo = array.pop();   //array = [1, 2], ultimo = 3
array.push(4);   //array = [1, 2, 3, 4]

/* SHIFT & UNSHIFT */
var array = [1, 2, 3];
var primero = array.shift();   // array = [2, 3], primero = 1
array.unshift(0);   // array = [0, 1, 2, 3]

/* REVERSE */
var array = [1, 2, 3];
array.reverse();   // array = [3, 2, 1]