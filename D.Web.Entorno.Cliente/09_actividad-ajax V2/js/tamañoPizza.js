//FUNCION TAMAÑO DE LA PIZZA

function inicioTamanoPizza(){
    var divTamano = document.createElement("div");
    divTamano.id = "pizza";

    document.getElementById("reiniciar").appendChild(divTamano);

    mostrarTamanoPizza();

}

function  mostrarTamanoPizza(){

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        respuestaTamano(this.responseText);
    }
  };
  xhttp.open("GET", "http://127.0.0.1:5501/json/tamano_pizza.json", true);
  xhttp.send();
  
}

function respuestaTamano(jsonDoc) {
    var objetoJsonTamano = JSON.parse(jsonDoc);
    console.log(objetoJsonTamano)
    var arrayTamano = objetoJsonTamano.pizza.tamano;
    var arrayMostrar = objetoJsonTamano.pizza.mostrarTamano;
    var arrayEspacio = objetoJsonTamano.pizza.espacio;

    for(let mostrarTamano of arrayMostrar){

    pTamano = document.createElement(mostrarTamano.elementP);
    textpTamano = document.createTextNode(mostrarTamano.titleTamano);
    pTamano.appendChild(textpTamano);
    pTamano.setAttribute("class", mostrarTamano.classObligado);

    document.getElementById("pizza").appendChild(pTamano);

    }

    for(let tamano of arrayTamano){
            radioTamano = document.createElement(tamano.elementInput);
            radioTamano.setAttribute("type", tamano.type);
            radioTamano.setAttribute("name", tamano.name);
            radioTamano.setAttribute("value", tamano.value);


            leftPizza = document.createElement(tamano.elementLeft);
            textoleftPizza = document.createTextNode(tamano.title);
            leftPizza.appendChild(textoleftPizza);

            br9 = document.createElement(tamano.espacio);

            document.getElementById("pizza").appendChild(radioTamano);
            document.getElementById("pizza").appendChild(leftPizza);
            document.getElementById("pizza").appendChild(br9);
    }
}