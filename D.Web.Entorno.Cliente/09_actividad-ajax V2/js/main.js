//--------------------------------------------------------------
var disponible = true;

//FUNCION QUE EJECUTA LAS DISTINTAS FUNCIONES QUE TENDRÁ EL HTML

window.onload = function(){

    textoPizzeria();
    obligaPizzeria();
    formularioPizzeria();
}
//--------------------------------------------------------------------

//FUNCION PARA MOSTRAR EL NOMBRE DE LA TIENDA

function textoPizzeria(){
    var divPizzeria= document.createElement("div");
    divPizzeria.id = "inicioPizza";

    document.body.appendChild(divPizzeria);

    textoInicioPizzeria();

}
function textoInicioPizzeria(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            inicioPizzeria(this.responseText);
        }
    };

    xhttp.open("GET", "http://127.0.0.1:5502/json/textoInicio.json", true);
    xhttp.send();
}
function inicioPizzeria(jsonDoc){

    var objetoJsonIncio = JSON.parse(jsonDoc);
    console.log(objetoJsonIncio);

    var arrayInicio = objetoJsonIncio.pizzeria.inicio;

    for(let inicio of arrayInicio){

        header1 = document.createElement(inicio.elementh1);
        textheader1 = document.createTextNode(inicio.title);
        header1.appendChild(textheader1);

        header1.setAttribute("class", inicio.classObligado);

        document.getElementById("inicioPizza").appendChild(header1);

    }
}
//----------------------------------------------------------------------

//FUNCION PARA MOSTRAR QUE LOS TEXTOS EN ROJO SON OBLIGATORIOS

function obligaPizzeria(){
    var divObligado= document.createElement("div");
    divObligado.id = "obligado";

    document.body.appendChild(divObligado);

    obligadoPizzeria();

}

function obligadoPizzeria(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            obligadoPizza(this.responseText);
        }
    };
    xhttp.open("GET", "http://127.0.0.1:5501/json/obligatorioPizza.json", true);
    xhttp.send();
}

function obligadoPizza(jsonDoc){

    var objetoJsonObligado = JSON.parse(jsonDoc);
    console.log(objetoJsonObligado);

    arrayObligado = objetoJsonObligado.pizzeriaObligado.obligatorio;

    for(let obligatorio of arrayObligado){

    textObligado = document.createElement(obligatorio.elementP);
    textObligatorio = document.createTextNode(obligatorio.title);
    textObligado.appendChild(textObligatorio);
    document.getElementById("obligado").appendChild(textObligado);

    }
}
//-------------------------------------------------------------------------

//FUNCION PARA INICIAR EL FORMULARIO

function formularioPizzeria(){
    xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            inicioFormu(this.responseText);
        }
    };

    xhttp.open("GET", "http://127.0.0.1:5501/json/formularioInicio.json", true);
    xhttp.send();
}
function inicioFormu(jsonDoc){
    var objetoJsonFormu = JSON.parse(jsonDoc);
    console.log(objetoJsonFormu);

    arrayFormu = objetoJsonFormu.pizzeriaFormulario.formu;

    for(let formu of arrayFormu){
        
        formuPizzeria = document.createElement(formu.elementform);
        formuPizzeria.id = formu.formid;
        document.body.appendChild(formuPizzeria);
    }
    inicioNombre()
    inicioDireccion();
    inicioTelefono();
    inicioEmail();
    inicioTamanoPizza();
    inicioExtra();
    inicioValidar();
    inicioProcesar();
    inicioLimpiar()
    inicioActualizar();
}


















































































