//FUNCIÓN PARA EL CAMPO NOMBRE

function inicioNombre(){
    var divNombre = document.createElement("div");
    divNombre.id = "idNombre";

    document.getElementById("reiniciar").appendChild(divNombre);

    nombre();

}



function nombre(){

    xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            mostrarNombre(this.responseText);
        }
    };

    xhttp.open("GET", "http://127.0.0.1:5501/json/formularioCampos.json", true);
    xhttp.send();
}

function mostrarNombre(jsonDoc){

    var objetoJsonNombre = JSON.parse(jsonDoc);

    console.log(objetoJsonNombre);

    arrayNombre = objetoJsonNombre.formuNombre;
    arrayEspacioNombre = objetoJsonNombre.espacio;

    for(let formuNombre of arrayNombre){

    var leftNombre = document.createElement(formuNombre.elementleft);
    var textoleftNombre = document.createTextNode(formuNombre.title);
    leftNombre.appendChild(textoleftNombre);
    var campoNombre = document.createElement(formuNombre.elementinput);

    leftNombre.setAttribute("class", formuNombre.classObligado);
    campoNombre.id = formuNombre.Nombreid;

    document.getElementById("idNombre").appendChild(leftNombre);
    document.getElementById("idNombre").appendChild(campoNombre);
    }

    for(let espacio of arrayEspacioNombre){

        br1 = document.createElement(espacio.espacio);
        br2 = document.createElement(espacio.espacio);

        document.getElementById("idNombre").appendChild(br1);
        document.getElementById("idNombre").appendChild(br2);
    }
}