package anadirLibrosCarro;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import usuarios.IntUsuarioDao;
import usuarios.Usuario;

public class AnadirDaoImpl implements IntAnadirLibrosDao {
	
	private Connection con;
	private PreparedStatement ps;
	private ResultSet rs;
	private String sql;
	
	public AnadirDaoImpl () {
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/libreria_virtual?serverTimezone=UTC", "root", "");
			System.out.println("conexion establecida");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Efisima");
			e.printStackTrace();
		}
	}
	
	public List<AnadirLibros> anadeLibros(int idAnadir) {
		ArrayList<AnadirLibros> anaLibros=new ArrayList<AnadirLibros>();
		
		try {
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){
				AnadirLibros analib=new AnadirLibros(rs.getInt("id"));
				anaLibros.add(analib);
			}
		}
		catch(SQLException ex){
			ex.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return anaLibros;
	}
}



