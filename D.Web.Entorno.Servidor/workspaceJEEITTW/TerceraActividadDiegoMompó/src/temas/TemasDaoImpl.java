package temas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import libros.Libros;
import usuarios.IntUsuarioDao;
import usuarios.Usuario;

public class TemasDaoImpl implements IntTemasDao{
	
	private Connection con;
	private PreparedStatement ps;
	private ResultSet rs;
	private String sql;
	
	public TemasDaoImpl() {
		
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/libreria_virtual?serverTimezone=UTC", "root", "");
			System.out.println("conexion establecida");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Efisima");
			e.printStackTrace();
		}
	}

	public List<Tema> recTemas() {
			sql="select * from temas";
			return temas(sql);
	}
	private List<Tema> temas(String sql) {
			ArrayList<Tema> temas = new ArrayList<Tema>();
			try {
				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
				while(rs.next()){
					
					Tema tem=new Tema(rs.getInt("id_tema"),
					rs.getString("tema"));
					temas.add(tem);
					
				}
					
			}
			catch(SQLException ex){
			ex.printStackTrace();
			}
			return temas;
		}
}


