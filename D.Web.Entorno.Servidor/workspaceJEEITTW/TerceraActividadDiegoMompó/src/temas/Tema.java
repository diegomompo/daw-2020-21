package temas;

import java.io.Serializable;

public class Tema implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id_Tema;
	private String tema;
	

	public Tema() {
		super();
	}

	public Tema(int id_Tema, String tema) {
		super();
		this.id_Tema = id_Tema;
		this.tema = tema;
	}

	public int getId_Tema() {
		return id_Tema;
	}

	public void setId_Tema(int id_Tema) {
		this.id_Tema = id_Tema;
	}

	public String getTema() {
		return tema;
	}

	public void setTema(String tema) {
		this.tema = tema;
	}
	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id_Tema;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tema other = (Tema) obj;
		if (id_Tema != other.id_Tema)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Tema [id_tema=" + id_Tema + ", tema=" + tema + "]";
	}
}
