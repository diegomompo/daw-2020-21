package usuarios;

import java.util.List;

import usuarios.Usuario;

public interface IntUsuarioDao {

	int insertarUsuario(Usuario usuario);
	Usuario findById(String usuario, String password);
	
	
}
