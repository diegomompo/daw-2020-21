package usuarios;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import usuarios.IntUsuarioDao;
import usuarios.Usuario;

public class UsuarioDaoImpl implements IntUsuarioDao {
	
	private Connection con;
	private PreparedStatement ps;
	private ResultSet rs;
	private String sql;
	
	public UsuarioDaoImpl () {
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/libreria_virtual?serverTimezone=UTC", "root", "");
			System.out.println("conexion establecida");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Efisima");
			e.printStackTrace();
		}
	}

	@Override
	public int insertarUsuario(Usuario usuario) {
		int filas = 0;
		sql = "INSERT INTO clientes VALUES(?,?,?,?,?)";
		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, usuario.getIdCliente());
			ps.setString(2, usuario.getUsuario());
			ps.setString(3, usuario.getPassword());
			ps.setString(4, usuario.getEmail());
			ps.setInt(5, usuario.getTelefono());
			
			filas = ps.executeUpdate();
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Usuario findById(String usuario, String password) {
		sql = "SELECT * FROM clientes WHERE usuario = ? AND password = ?";
		Usuario usuarios = null;
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, usuario);
			ps.setString(2, password);
			rs = ps.executeQuery();
					
			if(rs.next()) {
				usuarios = new Usuario();
				usuarios.setUsuario(rs.getString(1));
				usuarios.setPassword(rs.getString(2));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usuarios;
	}

}

