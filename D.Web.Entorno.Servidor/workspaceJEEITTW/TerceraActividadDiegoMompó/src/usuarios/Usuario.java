package usuarios;

import java.io.Serializable;

public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;
	private static int idCliente, telefono;
	private static String usuario, password, email;

	public Usuario() {
		super();
	}

	public Usuario(int idCliente, String usuReg, String passReg, String emaReg, int telReg) {
		super();
		this.idCliente = idCliente;
		this.telefono = telReg;
		this.usuario = usuReg;
		this.password = passReg;
		this.email = emaReg;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idCliente;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (idCliente != other.idCliente)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Usuario [idCliente=" + idCliente + ", telefono=" + telefono + ", usuario=" + usuario + ", password="
				+ password + "]";
	}
	
}

