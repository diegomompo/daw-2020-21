package ventas;

import java.io.Serializable;
import java.sql.Timestamp;

import usuarios.Usuario;

public class Venta implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id_venta, id_cliente;
	private float isbn;
	private Timestamp fecha = new Timestamp(new java.util.Date().getTime());

	public Venta() {
		super();
	}

	public Venta(int id_venta, int id_cliente, float isbn, Timestamp fecha) {
		super();
		this.id_venta = id_venta;
		this.id_cliente = id_cliente;
		this.isbn = isbn;
		this.fecha = fecha;
	}

	public int getId_venta() {
		return id_venta;
	}

	public void setId_venta(int id_venta) {
		this.id_venta = id_venta;
	}

	public int getId_cliente() {
		return id_cliente;
	}

	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}

	public float getIsbn() {
		return isbn;
	}

	public void setIsbn(float isbn) {
		this.isbn = isbn;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id_venta;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Venta other = (Venta) obj;
		if (id_venta != other.id_venta)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Venta [id_venta=" + id_venta + ", id_cliente=" + id_cliente + ", isbn=" + isbn + ", fecha=" + fecha
				+  "]";
	}
	
}

