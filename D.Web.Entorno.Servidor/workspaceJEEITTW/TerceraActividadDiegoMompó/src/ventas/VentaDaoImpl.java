package ventas;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import libros.Libros;
import usuarios.IntUsuarioDao;
import usuarios.Usuario;
import usuarios.UsuarioDaoImpl;

public class VentaDaoImpl implements IntVentaLibrosDao {
	
	private Connection con;
	private PreparedStatement ps;
	private ResultSet rs;
	private String sql;
	
	public VentaDaoImpl () {
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/libreria_virtual?serverTimezone=UTC", "root", "");
			System.out.println("conexion establecida");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Efisima");
			e.printStackTrace();
		}
	}
	
	public List<Venta> VentaLibros(int idCliente) {
		String sql="SELECT * FROM ventas WHERE id_cliente = " + idCliente;
		return ventas(sql);
	}
	public List<Venta> ventas(String sql) {
		ArrayList<Venta> venta=new ArrayList<Venta>();
		
		try {
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){
				Venta ven=new Venta(rs.getInt("id_venta"),
						rs.getInt("id_cliente"),
						rs.getFloat("isbn"),
						rs.getTimestamp("fecha"));
				
				venta.add(ven);
			}
		}
		catch(SQLException ex){
			ex.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return venta;
	}

	@Override
	public int InsertarVenta(Venta ventas) {
		sql = "INSERT INTO ventas(id_venta, id_cliente, isbn, fecha) values(?,?,?,?)";
		int filas = 0;
		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, ventas.getId_venta());
			ps.setInt(2, ventas.getId_cliente());
			ps.setDouble(3, ventas.getIsbn());
			ps.setTimestamp(4, ventas.getFecha());
			filas = ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return filas;
	}
}



