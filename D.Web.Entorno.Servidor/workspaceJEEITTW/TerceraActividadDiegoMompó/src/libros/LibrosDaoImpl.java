package libros;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import usuarios.IntUsuarioDao;
import usuarios.Usuario;

public class LibrosDaoImpl implements IntLibrosDao {
	
	private Connection con;
	private PreparedStatement ps;
	private ResultSet rs;
	private String sql;
	
	public LibrosDaoImpl () {
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/libreria_virtual?serverTimezone=UTC", "root", "");
			System.out.println("conexion establecida");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Efisima");
			e.printStackTrace();
		}
	}
	
	public List<Libros> recuperarLibros() {
			String sql="SELECT * FROM libros";
			return libros(sql);
	}
	
	public List<Libros> recuperarLibrosPorTema(int idTema) {
		String sql="SELECT * FROM libros WHERE id_tema = " + idTema;
			return libros(sql);
	}

	public List<Libros> libros(String sql) {
		ArrayList<Libros> libros=new ArrayList<Libros>();
		
		try {
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){
				Libros lib=new Libros(rs.getInt("isbn"),
				rs.getString("titulo"),
				rs.getString("autor"),
				rs.getFloat("precio"),
				rs.getInt("paginas"),
				rs.getInt("id_tema"));
				libros.add(lib);
			}
		}
		catch(SQLException ex){
			ex.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return libros;
	}
	
	public Libros findById(int ibanCarro) {
		sql = "select * from libros where titulo = " + ibanCarro;
		Libros libros = null;
		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, ibanCarro);
			rs = ps.executeQuery();
			if (rs.next()) {
				
				libros = new Libros();
				libros.setIban(rs.getInt(1));
				libros.setTitulo(rs.getString(2));
				libros.setAutor(rs.getString(3));
				libros.setPrecio(rs.getFloat(4));
				libros.setPaginas(rs.getInt(5));
				libros.setId_tema(rs.getInt(6));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return libros;
		
	}

}



