package libros;

import java.util.List;

import usuarios.Usuario;

public interface IntLibrosDao {

	List<Libros> recuperarLibros();
	List<Libros> recuperarLibrosPorTema(int idTema);
	Libros findById(int ibanCarro);


	
	
}
