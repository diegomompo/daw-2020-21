package libros;

import java.io.Serializable;

public class CarLibros implements Serializable {
	private static final long serialVersionUID = 1L;
	//private float precio;
	private String titulo, autor;

	public CarLibros() {
		super();
	}

	public CarLibros(String titulo, String autor) {
		super();
		this.titulo = titulo;
		this.autor = autor;
		//this.precio = precio;
	}


	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}
	
	/*public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}*/

	@Override
	public String toString() {
		return "Libros [titulo=" + titulo + ", autor=" + autor +  /*", precio=" + precio +*/ "]";
	}
	public static CarLibros Add(String tit, String aut) {
		return new CarLibros(tit, aut);
	}
}

