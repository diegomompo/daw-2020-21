package libros;

import java.io.Serializable;

public class Libros implements Serializable {
	private static final long serialVersionUID = 1L;
	private int iban;
	private float precio;
	private String titulo, autor;
	private int paginas, id_tema;

	public Libros() {
		super();
	}

	public Libros(int iban, String titulo, String autor, float precio, int paginas, int id_tema) {
		super();
		this.iban = iban;
		this.titulo = titulo;
		this.autor = autor;
		this.precio = precio;
		this.paginas = paginas;
		this.id_tema = id_tema;
	}

	public int getIban() {
		return iban;
	}

	public void setIban(int iban) {
		this.iban = iban;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}
	
	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public int getPaginas() {
		return paginas;
	}

	public void setPaginas(int paginas) {
		this.paginas = paginas;
	}

	public int getId_tema() {
		return id_tema;
	}

	public void setId_tema(int id_tema) {
		this.id_tema = id_tema;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + iban;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Libros other = (Libros) obj;
		if (iban != other.iban)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Libros [iban=" + iban + ", titulo=" + titulo + ", autor=" + autor +  ", precio=" + precio + ", paginas="
				+ paginas + ", id_tema=" + id_tema + "]";
	}
}

