


import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.swing.JCheckBox;

import anadirLibrosCarro.AnadirDaoImpl;
import anadirLibrosCarro.AnadirLibros;
import libros.CarLibros;
import libros.Libros;
import libros.LibrosDaoImpl;
import temas.Tema;
import temas.TemasDaoImpl;
import usuarios.Usuario;
import usuarios.UsuarioDaoImpl;
import ventas.Venta;
import ventas.VentaDaoImpl;

/**
 * Servlet implementation class Main
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Main() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String op = request.getParameter("op");
		
		switch(op) {
		
			case "validando":
				procesarValidar(request, response);
			break;
			case "registrando":
				procesarRegistrar(request, response);
			break;
			case "finalizandoRegistro":
				finalizarRegistrar(request, response);
			break;
			case "volviendoTemas":
				volverTemas(request, response);
			break;
			case "seleccionando":
				seleccionarTema(request, response);
			break;
			case "comprandoLibro":
				comprarLibro(request, response);
			break;
			case "eliminandoLibro":
				eliminaLibro(request, response);
			break;
			case "ejecutando":
				ejecutarVentas(request,response);
			break;
			case "volviendoLibros":
				volverLibros(request,response);
			break;
			default:
				System.out.println("opcion erronea : " + op);
		}
	}

	private void procesarValidar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String msj = null;
		
		String usu = request.getParameter("usuario");
		String pass =request.getParameter("pwd");
		
		UsuarioDaoImpl udao = new UsuarioDaoImpl();
		Usuario usuarios = udao.findById(usu, pass);
		TemasDaoImpl tdao = new TemasDaoImpl();
		List<Tema> temas;

		
		if(usuarios != null ) {
			temas = tdao.recTemas();
			session.setAttribute("usuarios", usuarios);
			session.setAttribute("temas", temas);
			request.getRequestDispatcher("Tema.jsp").forward(request, response);
		}else {
			msj = "El usuario o la contraseņa son incorrectos. Por favor, vuelva a intentarlo";
			request.setAttribute("msj", msj);
			request.getRequestDispatcher("Inicio.jsp").forward(request, response);

		}
	}

	private void procesarRegistrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("registro.jsp").forward(request, response);
	}
	
	private void finalizarRegistrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String usuReg = request.getParameter("usuarioReg");
		String passReg =request.getParameter("pwdReg");
		String emaReg = request.getParameter("emailReg");
		int telReg = Integer.parseInt(request.getParameter("telefonoReg"));
		
		UsuarioDaoImpl udao = new UsuarioDaoImpl();
		
		udao.insertarUsuario(new Usuario(5, usuReg, passReg, emaReg, telReg));

		
		request.getRequestDispatcher("Inicio.jsp").forward(request, response);
		
	}
	private void seleccionarTema(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		int idTema=Integer.parseInt(request.getParameter("tema"));
		
		LibrosDaoImpl ldao = new LibrosDaoImpl();
		List<Libros> libros;
		if(idTema==0){
			libros=ldao.recuperarLibros();
		}else {
			libros=ldao.recuperarLibrosPorTema(idTema);
		}
		sesion.setAttribute("libros", libros);
		request.getRequestDispatcher("libros.jsp").forward(request, response);
		
	}
	private void volverTemas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		TemasDaoImpl tdao = new TemasDaoImpl();
		List<Tema> temas;
		
		temas = tdao.recTemas();
		sesion.setAttribute("temas", temas);
		
		request.getRequestDispatcher("Tema.jsp").forward(request, response);
	}
	private void comprarLibro(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		
		LibrosDaoImpl ldao = new LibrosDaoImpl();
		VentaDaoImpl vdao = new VentaDaoImpl();
		Libros libros;
		List<Libros> anaLibros = (List<Libros>)sesion.getAttribute("libros");
		Usuario anaUsuario = (Usuario)sesion.getAttribute("usuarios");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		int id_cliente = Integer.parseInt(anaUsuario.getUsuario());
		
		int iban = Integer.parseInt(request.getParameter("id"));

		anaLibros = null;
		
		Venta venta = new Venta(0, id_cliente, iban, timestamp);
		
		vdao.InsertarVenta(venta);
		
		System.out.println(id_cliente);
		System.out.println(iban);
		System.out.println(venta);
		
		sesion.setAttribute("anaLibros", anaLibros);
		
		request.getRequestDispatcher("libros.jsp").forward(request, response);
	
}

	private void eliminaLibro(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion = request.getSession();
		
			List<Libros> eliminaLibros = (List<Libros>)sesion.getAttribute("anaLibros");
			
			eliminaLibros.remove(Integer.parseInt(request.getParameter("id")));
			
			sesion.setAttribute("anaLibros", eliminaLibros);
			request.getRequestDispatcher("libros.jsp").forward(request, response);	
	}
	private void ejecutarVentas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion=request.getSession();
		UsuarioDaoImpl udao = new UsuarioDaoImpl();
		VentaDaoImpl vdao = new VentaDaoImpl();
		
		List<Venta> ventas;

		Usuario usuario = (Usuario)sesion.getAttribute("usuarios");
		
		int venUsuario = Integer.parseInt(usuario.getUsuario());
		System.out.println(venUsuario);
		
		ventas = vdao.VentaLibros(venUsuario);
		request.setAttribute("ventas", ventas);
		
		System.out.println(venUsuario);
		System.out.println(ventas);
		
		request.getRequestDispatcher("ventas.jsp").forward(request, response);
		
	}
	private void volverLibros(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("libros.jsp").forward(request, response);		
	}

}
