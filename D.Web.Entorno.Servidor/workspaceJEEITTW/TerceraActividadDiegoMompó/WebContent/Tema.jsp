<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Librer�a virtual</title>
</head>
<body>
	<h1>Seleccionar Tema</h1>
	
	<form action="Main?op=seleccionando" method="post">
		<select name="tema">
			<option value="0">Todos</option>
				<c:forEach var="tem" items="${sessionScope.temas}" >
					<option value="${tem.id_Tema}">${tem.tema}</option>
				</c:forEach>
		</select>
		
		<button type="submit" class="btn btn-primary">Ver Libros</button>
	</form>
</body>
</html>