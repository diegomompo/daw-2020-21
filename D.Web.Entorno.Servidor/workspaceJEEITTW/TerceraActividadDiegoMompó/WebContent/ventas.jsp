<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Librer�a Virtual</title>
</head>
<body>
		<h3>Listado de libros</h3>
		<table border="1">
 			<tr><th>Numero de venta</th><th>Numero de cliente</th><th>ISBN</th><th>Fecha</th></tr>
 				<c:forEach var="ven" items="${requestScope.ventas}" varStatus="estado">
 					<td>${ven.id_venta}</td>
					<td>${ven.id_cliente}</td>
					<td>${ven.isbn}</td>
					<td>${ven.fecha}</td></tr>
 				</c:forEach>
		</table>
		<br/><br/>
		<form action="Main?op=volviendoTemas" method="post">
			<button type="submit" class="btn btn-primary">Otro Tema</button>
		</form>

</body>
</body>
</html>