<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Librer�a Virtual</title>
</head>
<body>
		<h3>Listado de libros</h3>
		<table border="1">
 			<tr><th></th><th>Titulo</th><th>Autor</th><th>Precio</th></tr>
 				<c:forEach var="lib" items="${sessionScope.libros}">
 					<td><a href="Main?op=comprandoLibro&id=${lib.iban}">Comprar</a></td>
 					<td>${lib.iban}</td>
 					<td>${lib.autor}</td>
					<td>${lib.titulo}</td>
					<td>${lib.precio}</td></tr>
 				</c:forEach>
		</table>
		<br/><br/>
		<h3>Carrito de libros</h3>
		<table border="1">
 			<tr><th></th><th>Titulo</th><th>Autor</th><th>Precio</th></tr>
 				<c:forEach var="clib" items="${sessionScope.anaLibros}" varStatus="estado">
 					<td><a href="Main?op=eliminandoLibro&id=${estado.index}">Eliminar</a></td>
 					<td>${clib.autor}</td>
					<td>${clib.titulo}</td>
					<td>${clib.precio}</td></tr>
 				</c:forEach>
		</table>
		<br/><br/>
		<form action="Main?op=volviendoTemas" method="post">
			<button type="submit" class="btn btn-primary">Otro Tema</button>
		</form>
		<br/><br/>		
		<form action="Main?op=ejecutando" method="post">
			<button type="submit" class="btn btn-primary">Ejecutar Compra</button>
		</form>
		<br/><br/>
</body>
</body>
</html>

