<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Librer�a Virtual</title>
</head>
<body>
	<h1>Registro para la liber�a</h1>
	
	<form action="Main?op=finalizandoRegistro" method="post">
	
		<div class="form-group">
			<label for="userReg">Usuario</label>
			<input type="text" name="usuarioReg" class="form-control" id="userReg">
		</div>
		
		<div class="form-group">
			<label for="passReg">Password</label>
			<input type="text" name="pwdReg" class="form-control" id="passReg">
		</div>
		
		<div class="form-group">
			<label for="emReg">Email</label>
			<input type="email" name="emailReg" class="form-control" id="emReg">
		</div>
		
		<div class="form-group">
			<label for="telReg">Telefono</label>
			<input type="number" name="telefonoReg" class="form-control" id="telReg">
		</div>
		
		<button type="submit" class="btn btn-primary">Grabar</button>
	</form>
</body>
</html>