<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Librer�a Virtual</title>
</head>
<body>
	<h1>Librer�a</h1>
	
	<%String msj = (String)request.getAttribute("msj");
		if(msj != null){%>
			<p><%=msj %></p>
	<%} %>
	
	
	<form action="Main?op=validando" method="post">
	
		<div class="form-group">
			<label for="user">Usuario</label>
			<input type="text" name="usuario" class="form-control" id="user">
		</div>
		
		<div class="form-group">
			<label for="pass">Password</label>
			<input type="text" name="pwd" class="form-control" id="pass">
		</div>
		
		<div class="form-group">
			<label for="remember">Recordar Usuario</label>
			<input type="checkbox" name="recordar" class="form-control" id="remember">
		</div>
		
		<button type="submit" class="btn btn-primary">Entrar</button>
	</form>
	
	<form action="Main?op=registrando" method="post">
		<button type="submit" class="btn btn-primary">Registrar</button>
	</form>
</body>
</html>