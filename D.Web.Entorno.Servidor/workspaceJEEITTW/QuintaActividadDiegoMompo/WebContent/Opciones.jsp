<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cajero Virtual</title>
</head>
<body>
	<h1>Men� de opciones del cajero</h1>
	
	<form action="Main?op=ingresando" method="post">
		<button type="submit" class="btn btn-primary">Ingresar</button>
	</form>
	<br/>
	<form action="Main?op=extrayendo" method="post">
		<button type="submit" class="btn btn-primary">Extraer</button>
	</form>
	<br/>
	<form action="Main?op=viendoMovimientos" method="post">
		<button type="submit" class="btn btn-primary">Ver Movimientos</button>
	</form>
	<br/>
	<form action="Main?op=realizandoTransferencia" method="post">
		<button type="submit" class="btn btn-primary">Transferencia</button>
	</form>
	<br/>
</body>
</html>