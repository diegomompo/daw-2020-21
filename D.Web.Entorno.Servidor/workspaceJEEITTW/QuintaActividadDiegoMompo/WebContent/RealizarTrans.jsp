<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cajero virtual</title>
</head>
<body>
<h1>Transferencia</h1>
	<form action="Main?op=finalizandoTransferencia" method="post">
		<div class="form-group">
			<label for="transfer">Introduce la cantidad que desea transferir:</label>
			<input type="number" name="transferencia_dinero" class="form-control" id="trasnfer">
		</div>
		<div class="form-group">
			<label for="destination">Introduce el numero de cuenta de destino:</label>
			<input type="number" name="numero_destino" class="form-control" id="destination">
		</div>
		<button type="submit" class="btn btn-primary">Hacer Transferencia</button>
	</form>
	<%String msj = (String)request.getAttribute("msj");
		if(msj != null){%>
			<p><%=msj %></p>
	<%} %>
</body>
</html>