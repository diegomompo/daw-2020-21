<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cajero virtual</title>
</head>
<body>
<h1>Ingresando</h1>
	<form action="Main?op=finalizandoIngresar" method="post">
		<div class="form-group">
			<label for="insert">Introduce la cantidad que desea ingresar:</label>
			<input type="number" name="ingresar_dinero" class="form-control" id="insert">
		</div>
		<button type="submit" class="btn btn-primary">Ingresar Cantidad</button>
	</form>
</body>
</html>