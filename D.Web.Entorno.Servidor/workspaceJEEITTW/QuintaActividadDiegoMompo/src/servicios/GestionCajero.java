package servicios;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import modelo.beans.Cuenta;
import modelo.beans.Movimiento;
import modelo.dao.CuentaDaoImpl;
import modelo.dao.ExtraerDaoImpl;
import modelo.dao.IngresarDaoImpl;
import modelo.dao.MovimientoDaoImpl;
import modelo.dao.TransferenciaDaoImpl;

@Path("cuentas")
public class GestionCajero {
	private IngresarDaoImpl idao;
	private CuentaDaoImpl cdao;
	private ExtraerDaoImpl edao;
	private TransferenciaDaoImpl tdao;
	private MovimientoDaoImpl mdao;

	public GestionCajero() {
		idao = new IngresarDaoImpl();
		cdao = new CuentaDaoImpl();
		edao = new ExtraerDaoImpl();
		tdao = new TransferenciaDaoImpl();
		mdao = new MovimientoDaoImpl();
	}
	
	@GET
	@Path("/valCuenta/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Cuenta findByNum(@PathParam("id") int num_cuenta) {
		Cuenta cuenta = cdao.findByNum(num_cuenta);
		return cuenta;
	}
	
	@PUT
	@Path("/ingSaldo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String IngSaldo(Cuenta cuentas) {
		int filas = idao.IngresarCantidadCuenta(cuentas);
		if(filas == 1) {
			return "Saldo actualizado correctamente";
		}
		else
			return "no se pudo actualizar el saldo";

	}
	@PUT
	@Path("/extSaldo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String ExtSaldo(Cuenta cuentas) {
		int filas = edao.ExtraerCantidadCuenta(cuentas);
		if(filas == 1) {
			return "Saldo actualizado correctamente";
		}
		else
			return "no se pudo actualizar el saldo";

	}
	
	@POST
	@Path("/ingMovi")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String IngMovi(Movimiento movimiento) {
		int filas = idao.IngresarCantidadMovimiento(movimiento);
		if(filas == 1) {
			return "Saldo introducidas correctamente";
		}
		else
			return "no no se pudo actualizar el saldo";

	}
	
	
	@PUT
	@Path("/transSaldo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String TransSaldo(Cuenta cuentas) {
		int filas = tdao.TransferenciaCantidadCuenta(cuentas);
		if(filas == 1) {
			return "Transferencia actualizada correctamente";
		}
		else
			return "no se ha podido realizar la transfernecia";

	}
	
	@GET
	@Path("/verMovi/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Movimiento> findByNumCuenta(@PathParam("id") int num_cuenta) {
		List<Movimiento> movi = mdao.findByCuenta(num_cuenta);
		return movi;
	}

}
