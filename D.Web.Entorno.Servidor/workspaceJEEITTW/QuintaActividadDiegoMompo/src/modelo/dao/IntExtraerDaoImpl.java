package modelo.dao;

import modelo.beans.Cuenta;
import modelo.beans.Movimiento;

public interface IntExtraerDaoImpl {
	int ExtraerCantidadCuenta(Cuenta cuenta);
	int ExtraerCantidadMovimiento(Movimiento moviemiento);
}
