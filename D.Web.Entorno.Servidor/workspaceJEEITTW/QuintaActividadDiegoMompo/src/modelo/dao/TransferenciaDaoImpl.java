package modelo.dao;

import modelo.beans.Cuenta;
import modelo.beans.Movimiento;

public class TransferenciaDaoImpl extends AbstractCuentaDao implements IntTransferenciaDaoImpl{

	@Override
	public int TransferenciaCantidadCuenta(Cuenta cuenta) {
		int filasCuenta = 0;
		
		try {
			tx.begin();
			em.merge(cuenta);
			tx.commit();
			filasCuenta = 1;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return filasCuenta;
	}
}



