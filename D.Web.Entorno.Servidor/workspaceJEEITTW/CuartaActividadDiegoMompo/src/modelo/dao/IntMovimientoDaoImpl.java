package modelo.dao;

import java.util.List;

import modelo.beans.Cuenta;
import modelo.beans.Movimiento;

public interface IntMovimientoDaoImpl {
	List<Movimiento> findByCuenta(int numeroCuenta);
}
