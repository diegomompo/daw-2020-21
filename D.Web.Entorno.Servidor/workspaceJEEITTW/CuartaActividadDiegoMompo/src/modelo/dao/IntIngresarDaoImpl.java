package modelo.dao;

import modelo.beans.Cuenta;
import modelo.beans.Movimiento;

public interface IntIngresarDaoImpl {
	int IngresarCantidadCuenta(Cuenta cuenta);
	int IngresarCantidadMovimiento(Movimiento moviemiento);
}
