package modelo.dao;

import modelo.beans.Cuenta;

public interface IntCuentaDaoImpl {
	Cuenta findByNum(int num_cuenta);
}
