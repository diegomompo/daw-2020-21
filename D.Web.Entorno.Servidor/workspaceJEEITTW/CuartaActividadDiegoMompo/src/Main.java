

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.beans.Cuenta;
import modelo.beans.Movimiento;
import modelo.dao.CuentaDaoImpl;
import modelo.dao.ExtraerDaoImpl;
import modelo.dao.IngresarDaoImpl;
import modelo.dao.MovimientoDaoImpl;
import modelo.dao.TransferenciaDaoImpl;

/**
 * Servlet implementation class Main
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final SimpleDateFormat fecha = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Main() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// SELECCIONAR OPCION
		
		String op = request.getParameter("op");
		
		switch(op) {
		
			case "entrando":
				procesarEntrar(request, response);
			break;
			case "ingresando":
				ingresar(request, response);
			break;
			case "finalizandoIngresar":
				finalizarIngresar(request, response);
			break;
			case "extrayendo":
				extraer(request, response);
			break;
			case "finalizandoExtraer":
				finalizarExtraer(request, response);
			break;
			case "realizandoTransferencia":
				realizarTransferencia(request, response);
			break;
			case "finalizandoTransferencia":
				finalizarTransferencia(request, response);
			break;
			case "viendoMovimientos":
				verMovimientos(request, response);
			break;
			case "volviendoOpciones":
				volverOpciones(request, response);
		}

	}
	
	//VALIDANDO EL NUMERO DE CUENTA

	private void procesarEntrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String msj = null;
		
		int cuen = Integer.parseInt(request.getParameter("cuenta_banco")); 
		
		CuentaDaoImpl cdao = new CuentaDaoImpl();
		Cuenta cuenta = cdao.findByNum(cuen);
		
		if(cuenta != null ) {
			session.setAttribute("cuenta", cuenta);
			request.getRequestDispatcher("Opciones.jsp").forward(request, response);
		}else {
			msj = "El numero de cuenta no existe. Por favor, vuelva a intentarlo";
			request.setAttribute("msj", msj);
			request.getRequestDispatcher("Inicio.jsp").forward(request, response);

		}
	}
	
	// SELECCIONANDO LA OPCION INGRESAR
	
	private void ingresar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("IngresarCantidad.jsp").forward(request, response);
	}
	
	// INGRESANDO LA CANTIDAD
	
	private void finalizarIngresar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		BigDecimal ingDin = new BigDecimal(request.getParameter("ingresar_dinero"));
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		IngresarDaoImpl idao = new IngresarDaoImpl();
		Cuenta iCuenta =  (Cuenta)session.getAttribute("cuenta");
		
		//A�ADIR EL DINERO AL SALDO ACTUAL
		
		BigDecimal saldoAnt = iCuenta.getSaldo();
	
		BigDecimal saldoAct = saldoAnt.add(ingDin);
		
		
		iCuenta.setSaldo(saldoAct);
		
		idao.IngresarCantidadCuenta(iCuenta);
	
		System.out.println(saldoAct);
		
		// A�ADIR MOVIMIENTO
		
		Movimiento mov = new Movimiento(0, ingDin, timestamp, "ingreso", iCuenta);
		idao.IngresarCantidadMovimiento(mov);

		request.getRequestDispatcher("Opciones.jsp").forward(request, response);
		
	}
	
	// SELECCIONANDO LA OPCION EXTRAER
	
	private void extraer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("ExtraerCantidad.jsp").forward(request, response);
	}
	
	// EXTRAYENDO LA CANTIDAD
	
	private void finalizarExtraer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		BigDecimal extDin = new BigDecimal(request.getParameter("extraer_dinero"));
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		ExtraerDaoImpl edao = new ExtraerDaoImpl();
		Cuenta eCuenta =  (Cuenta)session.getAttribute("cuenta");
		
		//RESTAR EL DINERO ACTUAL
		
		BigDecimal saldoAnt = eCuenta.getSaldo();
	
		BigDecimal saldoAct = saldoAnt.subtract(extDin);
		
		eCuenta.setSaldo(saldoAct);
		
		edao.ExtraerCantidadCuenta(eCuenta);
	
		System.out.println(saldoAct);
		
		//A�ADIR MOVIMIENTO
		
		Movimiento mov = new Movimiento(0, extDin, timestamp, "extraer", eCuenta);
		edao.ExtraerCantidadMovimiento(mov);
		
		request.getRequestDispatcher("Opciones.jsp").forward(request, response);
		
	}
	
	// OPCION DE REALIZAR LA TRANSFERENCIA
	
	private void realizarTransferencia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("RealizarTrans.jsp").forward(request, response);
	}
	
	// FINALIZAR TRANSFERENCIA
	
	private void finalizarTransferencia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String msj = null;
		
		BigDecimal transDin = new BigDecimal(request.getParameter("transferencia_dinero"));
		int cuenDes = Integer.parseInt(request.getParameter("numero_destino"));
		ExtraerDaoImpl edao = new ExtraerDaoImpl();
		IngresarDaoImpl idao = new IngresarDaoImpl();
		
		TransferenciaDaoImpl tdao = new TransferenciaDaoImpl();
		Cuenta eCuenta =  (Cuenta)session.getAttribute("cuenta");
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		CuentaDaoImpl cdao = new CuentaDaoImpl();
		Cuenta cuentaDestino = cdao.findByNum(cuenDes);
		
		
		if(cuentaDestino != null ) {
			session.setAttribute("cuentaDestino", cuentaDestino);
			Cuenta dCuenta = (Cuenta)session.getAttribute("cuentaDestino");

			BigDecimal saldoAntC1 = eCuenta.getSaldo();
			BigDecimal saldoAntC2 = dCuenta.getSaldo();
		
			BigDecimal saldoActC1 = saldoAntC1.subtract(transDin);
			BigDecimal saldoActC2 = saldoAntC2.add(transDin);
			
			eCuenta.setSaldo(saldoActC1);
			dCuenta.setSaldo(saldoActC2);
			
			tdao.TransferenciaCantidadCuenta(eCuenta);
			tdao.TransferenciaCantidadCuenta(dCuenta);
		
			System.out.println(saldoActC1);
			System.out.println(saldoActC2);
			
			Movimiento emov = new Movimiento(0, transDin, timestamp, "extraer", eCuenta);
			edao.ExtraerCantidadMovimiento(emov);
			Movimiento imov = new Movimiento(0, transDin, timestamp, "ingreso", dCuenta);
			idao.IngresarCantidadMovimiento(imov);
			
			request.getRequestDispatcher("Opciones.jsp").forward(request, response);
			
		}else {
			msj = "El numero de cuenta no existe. Por favor, vuelva a intentarlo";
			request.setAttribute("msj", msj);
			request.getRequestDispatcher("RealizarTrans.jsp").forward(request, response);
		}
		
	}
	
	//SELECCIONAR OPCION VER MOVIMIENTOS
	
	private void verMovimientos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		Cuenta mCuenta =  (Cuenta)session.getAttribute("cuenta");
		MovimientoDaoImpl mdao = new MovimientoDaoImpl();
		
		System.out.println(mCuenta.getNumeroCuenta());
		System.out.println(mCuenta.getSaldo());

		List<Movimiento> movi = new ArrayList<Movimiento>();
		movi = mdao.findByCuenta(mCuenta.getNumeroCuenta());
		
		session.setAttribute("movi", movi);
	
		
		
		request.getRequestDispatcher("Movimiento.jsp").forward(request, response);
	}
	
	// VOLVER AL MEN� OPCIONES
	
	private void volverOpciones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Opciones.jsp").forward(request, response);
		
	}

}
