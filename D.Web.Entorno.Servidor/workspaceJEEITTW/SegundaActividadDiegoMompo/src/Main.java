


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import usuarios.Usuario;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import contactos.Contacto;
import usuarios.Usuario;

/**
 * Servlet implementation class Main
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Main() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String op = request.getParameter("op");
			
			switch(op) {
			
				case "validando":
					procesandoValidar(request, response);
				break;
				case "registrandoContactos":
					registrarContacto(request, response);
				break;
				case "finalizandoContacto":
					finalizarContacto(request, response);
				break;
				case "viendoContactos":
					verContactos(request,response);
				break;
				case "eliminandoContacto":
					eliminarContacto(request,response);
				break;
				case "volviendoOpciones":
					retrocederOpciones(request, response);
				break;
				default:
					System.out.println("opcion erronea : " + op);

			}
	}



	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	private void procesandoValidar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String msj = null;
		
		ServletContext aplicacion = request.getServletContext();
		
		if(request.getParameter("usuarioContacto").equals("tomas") && 
				   request.getParameter("pwdContacto").equals("tomas")){
			
					Usuario usuarioContacto = Usuario.findByName();
					aplicacion.setAttribute("verUserContact", usuarioContacto);
					
					request.getRequestDispatcher("OpcionesContacto.jsp").forward(request, response);
					
				}else {
					
					msj = "El usuario o la contraseņa son incorrectos. Por favor, vuelva a intentarlo";
					request.setAttribute("msj", msj);
					request.getRequestDispatcher("inicio.jsp").forward(request, response);
					
				}
	}
	private void registrarContacto(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("NuevoContacto.jsp").forward(request, response);	
	}
	private void finalizarContacto(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext aplicacion = request.getServletContext();
		
		String nom = request.getParameter("nombreContacto");
		String dir = request.getParameter("direccionContacto");
		int tel = Integer.parseInt(request.getParameter("telefonoContacto"));
		
		Contacto contacto = Contacto.Find(nom, dir, tel);
		aplicacion.setAttribute("contacto", contacto);
		
		List<Contacto> listaContacto =  (List<Contacto>)aplicacion.getAttribute("listaContacto");
		
		if(listaContacto == null) {
			listaContacto = new ArrayList<Contacto>();
			listaContacto.add(contacto);
		}else {
			listaContacto.add(contacto);
		}
		
		aplicacion.setAttribute("listaContacto", listaContacto);
		
		request.getRequestDispatcher("OpcionesContacto.jsp").forward(request, response);	
	}
	private void verContactos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Contactos.jsp").forward(request, response);
	}
	
	private void eliminarContacto(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext aplicacion = request.getServletContext();
		List<Contacto> eliminarCont = (List<Contacto>)aplicacion.getAttribute("listaContacto");
		eliminarCont.remove(Integer.parseInt(request.getParameter("id")));
		System.out.println(Integer.parseInt(request.getParameter("id")));
		aplicacion.setAttribute("listaContacto", eliminarCont);
		request.getRequestDispatcher("Contactos.jsp").forward(request, response);

	}


	private void retrocederOpciones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("OpcionesContacto.jsp").forward(request, response);		
	}

}
		

