package contactos;

import java.io.Serializable;


public class Contacto implements Serializable{
	private static final long serialVersionUID = 1;
	private String nomContacto, dirContacto;
	private int telContacto;
	
	public Contacto() {
		super();
	}

	public Contacto(String nomContacto, String dirContacto, int telContacto) {
		super();
		this.nomContacto = nomContacto;
		this.dirContacto = dirContacto;
		this.telContacto = telContacto;
	}

	public String getNomContacto() {
		return nomContacto;
	}

	public void setNomContacto(String nomContacto) {
		this.nomContacto = nomContacto;
	}

	public String getDirContacto() {
		return dirContacto;
	}

	public void setDirContacto(String dirContacto) {
		this.dirContacto = dirContacto;
	}

	public int getTelContacto() {
		return telContacto;
	}

	public void setTelContacto(int telContacto) {
		this.telContacto = telContacto;
	}




	@Override
	public String toString() {
		return "Contacto [nomContacto=" + nomContacto + ", dirContacto=" + dirContacto + ", telContacto=" + telContacto
				+ "]";
	}
	
	public static Contacto 	Find(String nom, String dir, int tel){
		return new Contacto(nom, dir, tel);
	}
}
