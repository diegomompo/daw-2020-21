<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Contactos</title>
</head>
<body>
	<h1>Agenda de Contactos</h1>
	
	<p>${requestScope.msj}</p>
		
	<form action="Main?op=validando" method="post">
		<div class="form-group">
			<label for="userContact">Usuario</label>
			<input type="text"  name="usuarioContacto" class="form-control" id="userContact">
		</div>
		<div class="form-group">
			<label for="passContact">Contraseņa</label>
			<input type="password"  name="pwdContacto" class="form-control" id="passContact">
		</div>
		<button type="submit" class="btn btn-primary">Entrar</button>
	</form>

</body>

</body>
</html>