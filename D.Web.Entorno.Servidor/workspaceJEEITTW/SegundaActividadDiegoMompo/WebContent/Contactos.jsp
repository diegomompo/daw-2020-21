<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Contactos</title>
</head>
<body>
	<h1>Agenda de Contactos</h1>
	
	<h3>Contactos de  ${applicationScope.verUserContact.nombreContact}</h3>
	

	<ol>
		<c:forEach var="con" items="${applicationScope.listaContacto}" varStatus="estado">
			<li>${con.nomContacto} --- 
				${con.dirContacto} ---
				${con.telContacto}
				<a href="Main?op=eliminandoContacto&id=${estado.index}">Eliminar</a>
			</li>
		</c:forEach>
	</ol>
	
	<form action="Main?op=volviendoOpciones" method="post">
		<button type="submit" class="btn btn-primary">Volver</button>
	</form>
</body>
</html>