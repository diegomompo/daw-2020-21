package modelo.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.beans.Cliente;

public class ClienteDaoImpl implements IntClienteDao{

	@Override
	public List<Cliente> obtenerClientes() {
		List<Cliente> lista = new ArrayList<Cliente>();
		lista.add(new Cliente(100, "tomas", "tomas@itt.com", "tomasin"));
		lista.add(new Cliente(101, "eva", "e@itt.com", "evita"));
		lista.add(new Cliente(102, "javier", "j@itt.com", "javito"));
		return lista;
		
	}

	@Override
	public Cliente findById(int idCliente) {
		// TODO Auto-generated method stub
		return null;
	}

}
