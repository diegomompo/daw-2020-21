package controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.beans.Cliente;
import modelo.dao.ClienteDaoImpl;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 ServletContext miaplicacion = request.getServletContext();
		String opcion = request.getParameter("opcion");
		switch(opcion) {
		case "validar":
			procesarValidar(request, response);
			
			break;
			
		case "eliminar":
			procesarEliminar(request, response);
			
			break;
		case "cerrarSesion":
			HttpSession misesion = request.getSession();
			misesion.removeAttribute("cliente");
			misesion.invalidate();
			request.getRequestDispatcher("index.jsp").forward(request, response);
			
			break;
			
			default:
				System.out.println("opcion erronea : " + opcion);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	protected void procesarValidar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String mensaje = null;
		 HttpSession misesion = request.getSession();
//		 ClienteDaoImpl cdao = new ClienteDaoImpl();
//		 Cliente cliente = cdao.validarCliente();
		
		if (request.getParameter("correo").equals("tomas@itt.com") &&  
				 request.getParameter("pwd").equals("tomasin")) {
			 	Cliente cliente = Cliente.findById();
		//	 request.setAttribute("cliente", cliente);
			 	misesion.setAttribute("cliente", cliente);	
			 	ClienteDaoImpl cdao = new ClienteDaoImpl();
				List<Cliente> milista = cdao.obtenerClientes();
				misesion.setAttribute("lista", milista);
			 	
			 	
			 request.getRequestDispatcher("MenuBienvenida.jsp").forward(request, response);
			 
		 }else {
			 mensaje = "correo o password incorrectos miratelo majet@";
			 request.setAttribute("mensaje", mensaje);
			 request.getRequestDispatcher("index.jsp").forward(request, response);
			 
		 }
			 
	}
	
	protected void procesarEliminar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession misesion = request.getSession();
		List<Cliente> lista = (List<Cliente>)misesion.getAttribute("lista");
		lista.remove(Integer.parseInt(request.getParameter("id")));
		misesion.setAttribute("listaClientes", lista);
		request.getRequestDispatcher("ListarClientes.jsp").forward(request, response);
		
		
	}

}
