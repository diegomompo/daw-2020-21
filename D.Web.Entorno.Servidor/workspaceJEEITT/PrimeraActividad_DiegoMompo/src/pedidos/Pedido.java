package pedidos;

import java.io.Serializable;

public class Pedido implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String producto, color;
	private int unidades;
	
	public Pedido() {
		super();
	}
	
	public Pedido(String producto, int unidades, String color) {
		super();
		this.producto = producto;
		this.unidades = unidades;
		this.color = color;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getUnidades() {
		return unidades;
	}

	public void setUnidades(int unidades) {
		this.unidades = unidades;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((producto == null) ? 0 : producto.hashCode());
		result = prime * result + unidades;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedido other = (Pedido) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (producto == null) {
			if (other.producto != null)
				return false;
		} else if (!producto.equals(other.producto))
			return false;
		if (unidades != other.unidades)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pedido [producto=" + producto + ", unidades=" + unidades +  ", color=" + color + "]";
	}

	public static Pedido Add(String prod, int unid, String col) {
		return new Pedido(prod, unid, col);
	}
	
	
}
