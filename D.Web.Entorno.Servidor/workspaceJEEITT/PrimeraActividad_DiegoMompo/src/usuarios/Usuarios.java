package usuarios;

import java.io.Serializable;

public class Usuarios implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String nombre, password;
	
	public Usuarios() {
		super();
	}
	
	public Usuarios(String nombre, String password) {
		super();
		this.nombre = nombre;
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Usuarios))
			return false;
		Usuarios other = (Usuarios) obj;
		return true;
	}
	
	public String toString() {
		return "Usuarios [nombre=" + nombre + ", password=" + password + "]";
	}	
	
	public static Usuarios findByName() {
		return new Usuarios("tomas", "tomas");
	}
	
	
}
	
	
