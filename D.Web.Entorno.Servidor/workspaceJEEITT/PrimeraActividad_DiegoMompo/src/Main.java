
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pedidos.Pedido;
import usuarios.Usuarios;

/**
 * Servlet implementation class validarInicio
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Main() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String op = request.getParameter("op");
		switch(op) {
			case "validando":
				procesandoValidar(request, response);
				
			break;
			case "comprando":
				realizarPedido(request, response);
			break;
			case "finalizando":
				finalizarPedido(request, response);
			break;
			case "viendoCarrito":
				verCarrito(request, response);
			break;
			case "eliminando":
				eliminandoProducto(request, response);
			break;
			case "volviendoOpciones":
				retrocederOpciones(request, response);
			break;
			default:
				System.out.println("opcion erronea : " + op);
		}
	}
	
	protected void procesandoValidar(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
		
		String msj = null;
		
		HttpSession sesion = request.getSession();
		
		if(request.getParameter("usuario").equals("tomas") && 
		   request.getParameter("pwd").equals("tomas")){
			

			Usuarios  usuario = Usuarios.findByName();
			sesion.setAttribute("verUser", usuario);
			
			request.getRequestDispatcher("Opciones.jsp").forward(request, response);
			
		}else {
			
			msj = "El usuario o la contraseña son incorrectos. Por favor, vuelva a intentarlo";
			request.setAttribute("msj", msj);
			request.getRequestDispatcher("inicio.jsp").forward(request, response);
			
		}
	}
	protected void realizarPedido(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.getRequestDispatcher("Pedido.jsp").forward(request, response);
		
	}
	
	protected void finalizarPedido(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion = request.getSession();
		
		String prod = request.getParameter("producto");
		int  unid = Integer.parseInt(request.getParameter("unidades"));
		String color = request.getParameter("color");
		
		Pedido  pedido = Pedido.Add(prod, unid, color);
		sesion.setAttribute("pedido", pedido);
		
		
		List<Pedido> listaPedido = (List<Pedido>)sesion.getAttribute("listaPedido");
		
		if(listaPedido == null) {
			listaPedido = new ArrayList<Pedido>();
			listaPedido.add(pedido);	
		}
		else {
			listaPedido.add(pedido);
		}
		
		sesion.setAttribute("listaPedido", listaPedido);	
	
		
		request.getRequestDispatcher("Opciones.jsp").forward(request, response);
	}
	private void verCarrito(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.getRequestDispatcher("Carrito.jsp").forward(request, response);
	}

	private void eliminandoProducto(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		HttpSession sesion = request.getSession();
		List<Pedido> eliminarProd = (List<Pedido>)sesion.getAttribute("listaPedido");
		eliminarProd.remove(Integer.parseInt(request.getParameter("id")));
		sesion.setAttribute("listaPedido", eliminarProd);
		request.getRequestDispatcher("Carrito.jsp").forward(request, response);
		
	}

	private void retrocederOpciones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Opciones.jsp").forward(request, response);		
	}
}
