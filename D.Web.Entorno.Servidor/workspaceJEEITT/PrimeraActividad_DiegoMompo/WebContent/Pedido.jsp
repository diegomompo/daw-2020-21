<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Carro de la compra</title>
</head>
<body>
	<h1>Supermercado</h1>
	
		<form action="Main?op=finalizando" method="post">
			<div class="form-group">
				<label for="product">Producto</label>
				<input type="text"  name="producto" class="form-control" id="product">
			</div>
			<div class="form-group">
				<label for="units">Unidades</label>
				<input type="number"  name="unidades" class="form-control" id="units">
			</div>
			<div class="form-group">
				<label for="color">Color</label>
				<input type="text"  name="color" class="form-control" id="color">
			</div>
		
			<button type="submit" class="btn btn-primary">Comprar</button>
		</form>
		
		<form action="Main?op=volviendoOpciones" method="post">
			<button type="submit" class="btn btn-primary">Volver</button>
		</form>
		
		
</body>
</html>