<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Carro de la compra</title>
</head>
<body>
	<h1>Supermercado</h1>
	
	<%String msj = (String)request.getAttribute("msj");
		if(msj != null){%>
			<p><%=msj %></p>
	<%} %>
		
	<form action="Main?op=validando" method="post">
		<div class="form-group">
			<label for="user">Usuario</label>
			<input type="text"  name="usuario" class="form-control" id="user">
		</div>
		<div class="form-group">
			<label for="pass">Contraseña</label>
			<input type="password"  name="pwd" class="form-control" id="pass">
		</div>
		<button type="submit" class="btn btn-primary">Entrar</button>
	</form>
</body>
</html>