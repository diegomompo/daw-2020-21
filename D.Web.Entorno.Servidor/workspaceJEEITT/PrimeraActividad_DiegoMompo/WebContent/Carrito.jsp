<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="pedidos.Pedido"%>
<%@page import="usuarios.Usuarios"%>
<%@page import="java.util.Arrays"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Carro de la compra</title>
</head>
<body>
	<h1>Supermercado</h1>
	
	<%Usuarios usuario = (Usuarios)session.getAttribute("verUser");%>
	
	<h3>Carrito de <%= usuario.getNombre()%></h3>
	
	<%Pedido pedido = (Pedido)session.getAttribute("pedido");%>
	
	
	<% List<Pedido> lista = (List<Pedido>)session.getAttribute("listaPedido"); %>
	
	<ol>
		<%for (int i = 0; i < lista.size();i++){ %>
		
			<li><%=lista.get(i).getProducto() %> ----
				<%=lista.get(i).getUnidades() %> ----
				<%=lista.get(i).getColor() %> ----
			 <a href="Main?op=eliminando&id=<%=i %>">Eliminar</a></li>
	<%} %>
	</ol>
	
	<form action="Main?op=volviendoOpciones" method="post">
		<button type="submit" class="btn btn-primary">Volver</button>
	</form>
</body>
</html>