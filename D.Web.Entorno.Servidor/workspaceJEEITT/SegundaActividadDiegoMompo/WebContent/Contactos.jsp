<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Contactos</title>
</head>
<body>
	<h1>Agenda de Contactos</h1>
	
	
	<h3>Contactos de  ${sessionScope.verUserContact.nombreContact}</h3>
	
	
	<ol>
		<c:forEach var="con" items="${sessionScope.listaContacto}" varStatus=estado">
			<li>${con.nomContacto} --- 
				${con.dirContacto} ---
				${con.telContacto}
			</li>
		</c:forEach>
	</ol>
	
</body>
</html>