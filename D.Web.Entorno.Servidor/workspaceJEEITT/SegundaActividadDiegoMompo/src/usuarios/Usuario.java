package usuarios;

import java.io.Serializable;

public class Usuario implements Serializable {

	private static final long serialVersionUID = 1;
	private String nombreContact, p�sswordContacto;
	
	public Usuario() {
		super();
	}

	public Usuario(String nombreContact, String p�sswordContacto) {
		super();
		this.nombreContact = nombreContact;
		this.p�sswordContacto = p�sswordContacto;
	}

	public String getNombreContact() {
		return nombreContact;
	}

	public void setNombreContact(String nombreContact) {
		this.nombreContact = nombreContact;
	}

	public String getP�sswordContacto() {
		return p�sswordContacto;
	}

	public void setP�sswordContacto(String p�sswordContacto) {
		this.p�sswordContacto = p�sswordContacto;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Usuario))
			return false;
		Usuario other = (Usuario) obj;
		return true;
	}


	@Override
	public String toString() {
		return "Usuario [nombreContact=" + nombreContact + ", p�sswordContacto=" + p�sswordContacto + "]";
	}
	
	public static Usuario findByName() {
		return new Usuario("tomas", "tomas");
	}
	
}
