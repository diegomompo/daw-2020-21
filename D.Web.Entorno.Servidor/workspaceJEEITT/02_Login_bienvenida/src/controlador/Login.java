package controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.beans.Cliente;
import modelo.dao.ClienteDaoImpl;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String opcion = request.getParameter("opcion");
		switch(opcion) {
		case "validar":
			procesarValidar(request, response);
			
			break;
			
		case "listarUsuarios":
			procesarListarUsuarios(request, response);
			
			break;
			
			default:
				System.out.println("opcion erronea : " + opcion);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	protected void procesarValidar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String mensaje = null;
//		 ClienteDaoImpl cdao = new ClienteDaoImpl();
//		 Cliente cliente = cdao.validarCliente();
		
		if (request.getParameter("correo").equals("tomas@itt.com") &&  
				 request.getParameter("pwd").equals("tomasin")) {
			 	Cliente cliente = Cliente.findById();
			 request.setAttribute("cliente", cliente);
			 request.getRequestDispatcher("MenuBienvenida.jsp").forward(request, response);
			 
		 }else {
			 mensaje = "correo o password incorrectos miratelo majet@";
			 request.setAttribute("mensaje", mensaje);
			 request.getRequestDispatcher("index.jsp").forward(request, response);
			 
		 }
			 
	}
	
	protected void procesarListarUsuarios(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClienteDaoImpl cdao = new ClienteDaoImpl();
		List<Cliente> milista = cdao.obtenerClientes();
		request.setAttribute("listaClientes", milista);
		request.getRequestDispatcher("ListarClientes.jsp").forward(request, response);
		
		
	}

}
