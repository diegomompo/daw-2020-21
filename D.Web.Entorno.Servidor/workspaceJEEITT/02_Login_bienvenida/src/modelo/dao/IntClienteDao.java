package modelo.dao;

import java.util.List;

import modelo.beans.Cliente;

public interface IntClienteDao {
	
	public List<Cliente> obtenerClientes();

}
