<%@page import="modelo.beans.Cliente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%Cliente cliente = (Cliente)request.getAttribute("cliente"); %>
	<h1>Menu de opciones de <%=cliente.getNombre() %></h1>
	
	<ol>
		
		<li><a href="Login?opcion=listarUsuarios">Listar Usuarios</a></li>
		<li><a href="PrimerServlet?opcion=proyectosTerminados">Proyectos terminados</a></li>
		<li><a href="PrimerServlet?opcion=cerrarSesion">CerrarSesion</a></li>
		
	</ol>
</body>
</html>